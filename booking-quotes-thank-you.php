<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/booking_functions.php" );
require_once( __APP_PATH_CLASSES__.'/nps.class.php');
///$display_abandon_message = true;

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

$szMetaTitle = __BOOKING_RECEIPT_META_TITLE__ ;
$szMetaKeywords = __BOOKING_RECEIPT_META_KEYWORDS__;
$szMetaDescription = __BOOKING_RECEIPT_META_DESCRIPTION__;
$headerSearchFlag=1; 
require_once(__APP_PATH_LAYOUT__."/header_new.php");
$t_base = "BookingReceipt/";
$kBooking = new cBooking();  
$kConfig = new cConfig();
//$_SESSION['booking_id'] = 820;
if((int)$_SESSION['booking_id']<=0)
{
    $redirect_url = __HOME_PAGE_URL__ ;
    header("Location:".$redirect_url);
    die;
}

$postSearchAry = $kBooking->getExtendedBookingDetails($_SESSION['booking_id']); 

if($postSearchAry['iPaymentType']==__TRANSPORTECA_PAYMENT_TYPE_3__) 
{ 
    //Only in case of Bank Transfer we send E-mail from this page. 

    $kForwarderContact= new cForwarderContact();
    $kUser = new cUser();
    $kWHSSearch = new cWHSSearch();
    confirmBookingForBankTransfer($postSearchAry); 
}

$postSearchAry = $kBooking->getExtendedBookingDetails($_SESSION['booking_id']); 

if(($postSearchAry['idShipperConsignee']<=0) || ($postSearchAry['idForwarder']<=0) || ($postSearchAry['idWarehouseTo']<=0) || ($postSearchAry['idWarehouseFrom']<=0))
{
    $redirect_url = __HOME_PAGE_URL__ ;
    header("Location:".$redirect_url);
    die;
    /*
    ?>
    <div id="change_price_div">
    <?
    display_booking_notification($redirect_url);
    echo "</div>";
*/
} 
$kForwarder = new cForwarder();
$customerServiceEmailAry = array();
$idForwarder = $postSearchAry['idForwarder'] ;
$customerServiceEmailAry = $kForwarder->getForwarderCustomerServiceEmail($idForwarder); 
$szWareHouseFromStr = '';
$szWareHouseMapStr = '';


if($postSearchAry['szPaymentStatus']=='Completed')
{
    $iLanguage = getLanguageId();
    if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__)
    {
        $szBankTransferDueDateTime = strtotime($postSearchAry['dtCutOff']) - ($postSearchAry['iBookingCutOffHours']*60*60);

        $iMonth = date('m',$szBankTransferDueDateTime); 
        $szBankTransferDueDateTime = date('j.',$szBankTransferDueDateTime);  

        $szMonthName = getMonthName($iLanguage,$iMonth);
        $szBankTransferDueDateTime .=" ".$szMonthName ; 
    }
    else
    {
        $szBankTransferDueDateTime = strtotime($postSearchAry['dtWhsCutOff']) - ($postSearchAry['iBookingCutOffHours']*60*60);

        $iMonth = date('m',$szBankTransferDueDateTime); 
        $szBankTransferDueDateTime = date('j.',$szBankTransferDueDateTime);   

        $szMonthName = getMonthName($iLanguage,$iMonth);
        $szBankTransferDueDateTime .=" ".$szMonthName ;
    }  
	
    $dtAvailableDateTime = strtotime($postSearchAry['dtAvailable']) ;
    $dtAvailableString = date('j.',$dtAvailableDateTime); 

    $iAvailableMonth = date('m',$dtAvailableDateTime); 
    $szAvailableMonthName = getMonthName($iLanguage,$iAvailableMonth);
    $dtAvailableString .=" ".$szAvailableMonthName ;

    $idCurrency = $postSearchAry['idCustomerCurrency'];
    $currencyDetailsAry = array();
    $currencyDetailsAry = $kBooking->loadCurrencyDetails($idCurrency); 

    if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__)
    {
        $idWarehouseFromCountry = $postSearchAry['idWarehouseFromCountry'];
    }
    else
    {
        $idWarehouseFromCountry = $postSearchAry['idShipperCountry_pickup'];
    }

    $kConfig->loadCountry($idWarehouseFromCountry);
    $szCountryName = $kConfig->szCountryName ;

    if($postSearchAry['iInsuranceIncluded']==1)
    {
        $fTotalPriceCustomerCurrency = ($postSearchAry['fTotalPriceCustomerCurrency'] + $postSearchAry['fTotalInsuranceCostForBookingCustomerCurrency']); 
    }
    else
    {
        $fTotalPriceCustomerCurrency =  $postSearchAry['fTotalPriceCustomerCurrency'];
    }
    
//    if($iLanguage==__LANGUAGE_ID_DANISH__)
//    { 
//        $szBookingAmountStr =  $postSearchAry['szCurrency']." ". number_format((float)$fTotalPriceCustomerCurrency,0,'.','.');
//    }
//    else
//    { 
//        $szBookingAmountStr = $postSearchAry['szCurrency']." ".number_format((float)$fTotalPriceCustomerCurrency,0,'.',',');
//    }
    
    $szBookingAmountStr = $postSearchAry['szCurrency']." ".  number_format_custom($fTotalPriceCustomerCurrency,$iLanguage,0);
?> 
<div id="hsbody-2"> 
	<div class="hsbody-right">
		<h2 class="thank-you-page-heading"><?=t($t_base.'fields/your_booking_recieved');?></h2>
		<div class="booking-reciept-main-container clearfix" id="booking_details_container">
			<div class="pdf-image-container">		
				<?php if($postSearchAry['iPaymentType']==__TRANSPORTECA_PAYMENT_TYPE_3__) { ?>		
					<a href="<?php echo __BASE_URL__."/viewInvoice/".$postSearchAry['id']."/"; ?>" target="_blank"><img src="<?php echo __BASE_STORE_IMAGE_URL__."/invoice-icon.png"; ?>"></a>
				<?php } else {?>
					<a href="<?php echo __BASE_URL__."/viewInvoice/".$postSearchAry['id']."/"; ?>" target="_blank"><img src="<?php echo __BASE_STORE_IMAGE_URL__."/invoice-icon.png"; ?>"></a>
					<a href="<?php echo __BASE_URL__."/viewBookingConfirmation/".$postSearchAry['id']."/"; ?>"  target="_blank"><img src="<?php echo __BASE_STORE_IMAGE_URL__."/booking-icon.png"; ?>"></a>
				<?php } ?> 
			</div>
			<div class="oh even first icons">
				<p><?php echo t($t_base.'fields/booking');?> - <?php echo t($t_base.'fields/your_details_recieved');?></p>
			</div>
			<div class="oh even icons">
				<p><?php echo t($t_base.'fields/invoice');?> - <?php echo t($t_base.'fields/sent_to')." ".$postSearchAry['szEmail'];?></p>
			</div>
			<?php if($postSearchAry['iPaymentType']==__TRANSPORTECA_PAYMENT_TYPE_3__) { $class="even"; ?>
				<div class="oh odd">
					<p><?php echo t($t_base.'fields/payment')." - ".t($t_base.'fields/please_make_transfer')." ".$szBookingAmountStr." ".t($t_base.'fields/payment_latest_by')." ".$szBankTransferDueDateTime; ?></p>
					<div class="bank-details-box">
						<p class="clearfix"><span class="left"><?php echo t($t_base.'fields/bank').": "; ?></span><span><?php echo $currencyDetailsAry['szBankName']; ?></span></p>
						<?php if(!empty($currencyDetailsAry['szSortCode'])){?>
							<p class="clearfix"><span class="left"><?php echo t($t_base.'fields/sort_code').": "; ?></span><span><?php echo $currencyDetailsAry['szSortCode']; ?></span></p>
						<?php } ?>
						<p class="clearfix"><span class="left"><?php echo t($t_base.'fields/account_number').": "; ?></span><span><?php echo $currencyDetailsAry['szAccountNumber']; ?></span></p>
						<p class="clearfix"><span class="left"><?php echo t($t_base.'fields/swift').": "; ?></span><span><?php echo $currencyDetailsAry['szSwiftNumber']; ?></span></p>
						<p class="clearfix"><span class="left"><?php echo t($t_base.'fields/name_on_account').": "; ?></span><span><?php echo $currencyDetailsAry['szNameOnAccount']; ?></span></p>
						<p class="clearfix"><span class="left"><?php echo t($t_base.'fields/reference_on_transfer').": "; ?></span><span><?php echo $postSearchAry['szBookingRef']; ?></span></p>						
					</div>
				</div>
				<div class="oh even">
					<p><?php echo t($t_base.'fields/confirmation');?> - <?php echo t($t_base.'fields/we_send_booking_confirmation')." ".$postSearchAry['szEmail'];?></p>
				</div>
			<?php }else { $class = 'odd'; ?>
				<div class="oh even">
					<p><?php echo t($t_base.'fields/payment')." - ".t($t_base.'fields/completed'); ?></p>
				</div>
			<?php }?>
			<?php if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__){?>			
				<div class="oh <?php echo $class; ?>">
					<p><?php echo t($t_base.'fields/pick-up');?> - <?php echo t($t_base.'fields/forwarder_will_collect')." ".$szCountryName; ?></p>
				</div>
			<?php }else{ ?>
				<div class="oh <?php echo $class; ?>">
					<p><?php echo t($t_base.'fields/departure');?> - <?php echo t($t_base.'fields/forwarder_will_recieve')." ".$szCountryName;?></p>
				</div>
			<?php } ?>
			<div class="oh even last">
				<p><?php echo t($t_base.'fields/delivery');?> - <?php echo t($t_base.'fields/receive_your_shipment')." ".$dtAvailableString ; ?></p>
			</div>
		</div>	 
	</div>
	<?php
	$kNPS = new cNPS();
	if(!$kNPS->getCustomerFeedBackDay($_SESSION['user_id']))
	{
		?>
		<div class="booking-feedback-container" id="booking-feedback-container-div">			
			<?php 
				echo customer_feedback_form();
			?>
		</div>
<?php } else {  ?>
<script type="text/javascript">
	$("#header-search").attr('style','top: 0px; bottom: auto; display: block;'); 
</script>
<?php }?>
</div>
		
<!-- Google Code for purchase Conversion Page -->
<script type="text/javascript">
	/* <![CDATA[ */
	vargoogle_conversion_id = 1001820024;
	vargoogle_conversion_language = "en";
	vargoogle_conversion_format = "3";
	vargoogle_conversion_color = "ffffff";
	vargoogle_conversion_label = "omJXCKCouQMQ-J7a3QM";
	vargoogle_conversion_value = 0;
	/* ]]> */
</script>
<script type="text/javascript" src="https://www.googleadservices.com/pagead/conversion.js"></script>
<noscript>
	<div style="display:inline;"><img height="1" width="1" style="border-style:none;" alt="" src="https://www.googleadservices.com/pagead/conversion/1001820024/?value=0&amp;label=omJXCKCouQMQ-J7a3QM&amp;guid=ON&amp;script=0"/></div>
</noscript>
		
<?php } else if($postSearchAry['szPaymentStatus']=='Failed') {  unset($_SESSION['amount_mismatch']); ?>
	<div id="hsbody">			
		<p><h5><?=t($t_base.'fields/thank_you_booking');?></h5></p>
		<p> <?=t($t_base.'fields/booking_ref_is');?>: <?=$postSearchAry['szBookingRef']?></p><br>
		<p> <? echo t($t_base.'fields/your_paid_amount_does_not_match_with_booking_amount');?>.</p><br>
	</div>
<?php } else {?>
	<div id="hsbody">			
		<p><h5><?=t($t_base.'fields/thank_you_booking');?></h5></p>
		<p> <?=t($t_base.'fields/booking_ref_is');?>: <?=$postSearchAry['szBookingRef']?></p><br>
		<p> <? echo t($t_base.'fields/your_payment_status_is_in_pending_state');?></p><br>
	</div>		
<?php } ?> 
<?php
$invoiceUrl=__BASE_URL__."/viewInvoice/".$postSearchAry['id']."/";
$bookingConfirmationUrl=__BASE_URL__."/viewBookingConfirmation/".$postSearchAry['id']."/";

$_SESSION['booking_id'] = '';
unset($_SESSION['booking_id']);
$_SESSION['booking_register_shipper_id'] = '';
$_SESSION['booking_register_consignee_id'] = '';
unset($_SESSION['booking_register_shipper_id']);
unset($_SESSION['booking_register_consignee_id']);

$_SESSION['logged_in_by_booking_process']='';
unset($_SESSION['logged_in_by_booking_process']);
		
$iThankYouPageAdword = 1;
require_once(__APP_PATH_LAYOUT__."/footer_new.php");
$kNPS = new cNPS();
if(!$kNPS->getCustomerFeedBackDay($_SESSION['user_id']))
{
?>
<script type="text/javascript">
       var elements = document.getElementsByTagName('a');
                  //alert(elements[0].href);
                  for(var i = 0, len = elements.length; i < len; i++) 
                  {
                        //alert(elements[28].href);
                        if(elements[i].href!='' && elements[i].href!='javascript:void(0)' && elements[i].href!='javascript:void(0);' && elements[i].href!='<?=$invoiceUrl?>' && elements[i].href!='<?=$bookingConfirmationUrl?>')
                        {
                        	 // alert(elements[i].href);	
                              //$(elements[i]).attr('onClick','checkPageRedirectionBulkService_feedback(\''+elements[i].href+'\');');
                              //$(elements[i]).attr('href','javascript:void(0);');
                  		}
                  }
</script>
<? }?>