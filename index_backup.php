<?
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
$display_abandon_message = false;


$szURL = $_SERVER['REQUEST_URI'] ;
$kExplain= new cExplain();

$szLanguage = sanitize_all_html_input($_REQUEST['lang']);
$iLanguage = getLanguageId($szLanguage);

if(!empty($szURL))
{
	$szURL = str_replace("/","",$szURL);
	if(!empty($szURL))
	{
		$landingPageDataAry = $kExplain->getLandingPageDataByUrl($szURL);
	}
}
if(empty($landingPageDataAry))
{
	//fetching default Landing page.
	$landingPageDataAry = $kExplain->getLandingPageDataByUrl(false,true,false,$iLanguage);
}

if(!empty($landingPageDataAry['szMetaTitle']))
{
	$szMetaTitle = $landingPageDataAry['szMetaTitle'] ;
}
else 
{
	$szMetaTitle = __LANDING_PAGE_META_TITLE__ ;
}

if(!empty($landingPageDataAry['szMetaKeywords']))
{
	$szMetaKeywords = $landingPageDataAry['szMetaKeywords'] ;
}
else
{
	$szMetaKeywords = __LANDING_PAGE_META_KEYWORDS__;
}

if(!empty($landingPageDataAry['szMetaDescription']))
{
	$szMetaDescription = $landingPageDataAry['szMetaDescription'];
}
else
{
	$szMetaDescription = __LANDING_PAGE_META_DESCRIPTION__;
}

require_once(__APP_PATH_LAYOUT__."/header.php");
$t_base = "LandingPage/";

$kConfig = new cConfig();
$kBooking = new cBooking();
$countryAry=$kConfig->getAllCountries(false,$iLanguage);

$szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_random_num'])); 
$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
if((int)$idBooking>0)
{
	$postSearchAry = $kBooking->getBookingDetails($idBooking);
}
if($_REQUEST['err']==1)
{
	$kBooking->addExpactedServiceData($idBooking);
	$landing_page_url = __LANDING_PAGE_URL__ ;
	$idExpactedService = $kBooking->idExpactedService ;
	?>
	<script type="text/javascript">
		addPopupScrollClass('all_available_service');
	</script>
	<div id="all_available_service" style="display:block;">	
	<?
	echo display_no_service_available_popup($t_base,$landing_page_url,$idBooking,$idExpactedService,$kBooking,$postSearchAry);
	echo "</div>";
}

if(!empty($_REQUEST['country_id_pair']))
{
	$tempCountryAry = explode("__",$_REQUEST['country_id_pair']);
	$postSearchAry['idOriginCountry'] = $tempCountryAry[0];
	$postSearchAry['idDestinationCountry'] = $tempCountryAry[1];
	
	$iAutoSubmitFlag = true;
}
?>
		<div id="hsbody" class="search-option">
			<div class="search-new">
				<h6 align="center"><?=$landingPageDataAry['szTextA']?></h6>
				<form action="" id="start_search_form" name="start_search_form" method="post">
					<div class="fields">
						<p>
							<select name="landingPageAry[szOriginCountry]" class="styled" id="szOriginCountry">
								<option value=""><?=t($t_base.'fields/shipping_from_index');?></option>
								<?
								 	if(!empty($countryAry['fromCountry']))
								 	{
								 		foreach($countryAry['fromCountry'] as $countryArys)
								 		{
								 			?>
								 				<option value="<?=$countryArys['id']?>" <? if($postSearchAry['idOriginCountry']==$countryArys['id']){?> selected <? }?>><?=substr($countryArys['szCountryName'],0,25)?></option>
								 			<?
								 		}
								 	}
								 ?>
							</select>
						</p>
						<p>
							<select name="landingPageAry[szDestinationCountry]" class="styled" onkeyup="on_enter_key_press(event,'compare_validate_home_page');" id="szDestinationCountry">
								<option value=""><?=t($t_base.'fields/shipping_to_index');?></option>
								 <?
								 	if(!empty($countryAry['toCountry']))
								 	{
								 		foreach($countryAry['toCountry'] as $countryArys)
								 		{
								 			?>
								 				<option value="<?=$countryArys['id']?>" <? if($postSearchAry['idDestinationCountry']==$countryArys['id']){?> selected <? }?>><?=substr($countryArys['szCountryName'],0,25)?></option>
								 			<?
								 		}
								 	}
								 ?>
							</select>
							<input name="landingPageAry[szBookingRandomNum]" type="hidden" value="<?=$szBookingRandomNum?>" id="szBookingRandomNum">
							<input name="landingPageAry[iCheck]" type="hidden" value="1" id="iCheck">
							<input name="landingPageAry[idLandingPage]" type="hidden" value="<?php echo $landingPageDataAry['id'];?>" id="idLandingPage">
						</p>
						<p>
						<input type="hidden" value="<?=$landingPageDataAry['szTextB']?>" name="compare_button_name" id="compare_button_name">
						<input type="hidden" value="<?=t($t_base.'fields/searching_services');?>" name="compare_button_name_2" id="compare_button_name_2">						
						<a href="javascript:void(0);" class="orange-button2" onclick = "compare_validate_home_page()" tabindex="3" id="campare_button_req_page"><span><?=$landingPageDataAry['szTextB']?></span></a>
						</p>
					</div>
				</form>
				<div class="text">
					<p class="book_in_minutes"><?=$landingPageDataAry['szTextC']?></p>
					<?php echo $landingPageDataAry['szTextD'];?>		
				</div>
			</div>
			<div class="home-new">
				<h2><?php echo $landingPageDataAry['szTextE'];?></h2>
				<p><?php echo $landingPageDataAry['szTextF'];?></p>
				
				<?php
				
					if($iLanguage==__LANGUAGE_ID_DANISH__)
					{
						$szClassName = 'class="intro_video_danish"';
					}
					else
					{
						$szClassName = 'class="intro_video"';
					}
				?>			
				<div <?php echo $szClassName; ?>>	
				<? if((int)$_SESSION['user_id']==0){
				?>			
				<div align="center" style="width:500px;height:280px;" id="evp-c4e48429cc58c6896f1cb705d41ec6da-wrap" class="evp-video-wrap"></div>
				<script type="text/javascript" src="https://www.transporteca.com/evp/framework.php?div_id=evp-c4e48429cc58c6896f1cb705d41ec6da&id=dHJhbnNwb3J0ZWNhLWludHJvZHVjdGlvbi0xLm1wNA%3D%3D&v=1352203827&profile=default"></script>
				<script type="text/javascript"><!--
				_evpInit('dHJhbnNwb3J0ZWNhLWludHJvZHVjdGlvbi0xLm1wNA==[evp-c4e48429cc58c6896f1cb705d41ec6da]');//-->
				</script>
				<? }else {
				?>
				<div align="center" style="width:500px;height:280px;" id="evp-0b6948c7430907f52b498c4a81f56ffc-wrap"	class="evp-video-wrap"></div>
				<script type="text/javascript" src="https://www.transporteca.com/evp/framework.php?div_id=evp-0b6948c7430907f52b498c4a81f56ffc&id=dHJhbnNwb3J0ZWNhLWludHJvZHVjdGlvbi1pbmZvcm0tYS1mcmllbmQtMS5tcDQ%3D&v=1352273443&profile=default"></script>
				<script type="text/javascript"><!--
					_evpInit('dHJhbnNwb3J0ZWNhLWludHJvZHVjdGlvbi1pbmZvcm0tYS1mcmllbmQtMS5tcDQ=[evp-0b6948c7430907f52b498c4a81f56ffc]');//-->
				</script>
				<? }?>
			  <!-- <img src="images/see-yourself-transporteca.jpg" alt="" title="" /> -->
			 </div>					
				<h3><?php echo $landingPageDataAry['szTextG'];?></h3>
				<div class="benefits">					
					<div class="first common">
						<h5><?php echo $landingPageDataAry['szTextH'];?></h5>
						<p><?php echo $landingPageDataAry['szTextI'];?></p>
					</div>
					<div class="common">
						<h5><?php echo $landingPageDataAry['szTextJ'];?></h5>
						<p><?php echo $landingPageDataAry['szTextK'];?></p>
					</div>
					<div class="last common">
						<h5><?php echo $landingPageDataAry['szTextL'];?></h5>
						<p><?php echo $landingPageDataAry['szTextM'];?></p>
					</div>
					<?php if((int)$_SESSION['user_id']<=0){ /*?>
						<p align="center" class="button"><a href="<?=__BASE_URL__.'/createAccount/'?>" class="register-button">&nbsp;</a></p>
					<?php */ } else {?>
					<p align="center" class="button"><a href="javascript:void(0);" style="text-decoration:none;" onclick="display_share_link_popup()" class="share-this-button">&nbsp;</a></p>
					<? }?>
				</div>
			</div>
		</div>
		<div id="share_link_popup_div" style="display:none;"></div>
		<div id="error_popup" style="display:none;"></div>
		<div id="all_available_service" style="display:none;"></div>
		
		<?

	if(!empty($_SESSION['multi_user_success_message']))
	{
		?>
		<div id="show_invite_msg">
		<div id="popup-bg"></div>
			<div id="popup-container">
				<div class="popup signin-popup signin-popup-verification">
				<?=$_SESSION['multi_user_success_message']?><br />
				<p align="center"><a href="javascript:void(0)" onclick="close_invite_confirm_popup()" class="button1"><span><?=t($t_base.'fields/close');?></span></a></p>	
				</div>
			</div>	
			</div>
		<?
		$_SESSION['multi_user_success_message'] = '';
		$_SESSION['multi_user_key'] = '';
		unset($_SESSION['multi_user_key']);
		unset($_SESSION['multi_user_success_message']);
	}
?>
<script type="text/javascript">
var inner_html = "<span><?=$landingPageDataAry['szTextB']?></span>";
$("#campare_button_req_page").html(inner_html);
$("#campare_button_req_page").attr("onclick","");
$("#campare_button_req_page").unbind("click");
$("#campare_button_req_page").click(function(){compare_validate_home_page()});
$("#campare_button_req_page").attr('style',"opacity:1");
</script>
<?
//echo previewLandingPage();
if($iAutoSubmitFlag)
{
	?>
	<script type="text/javascript">
		compare_validate_home_page();
	</script>
	<?php
}	
if($_REQUEST['err']==2)
{
?>
<script type="text/javascript">
ajaxLogin('<?=$redirect_url?>');
</script>
<?
}

require_once(__APP_PATH_LAYOUT__."/footer.php");
?>	