<?php
ob_start();
session_start();

if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
require_once(__APP_PATH_LAYOUT__."/ajax_header.php");
$t_base = "SelectService/";


$kConfig=new cConfig();
$kWHSSearch=new cWHSSearch();
$kBooking = new cBooking();
$tempResultAry=array();
$searchResultAry=array();

$szBookingRandomNum = sanitize_all_html_input(trim($_POST['selectServiceAry']['szBookingRandomNum'])); 
$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);

if((int)$idBooking>0)
{
	$searchResultAry=array();
	$tempResultAry = $kWHSSearch->getSearchedDataFromTempData($idBooking);
	$searchResultAry = unserialize($tempResultAry['szSerializeData']);
	$postSearchAry = $kBooking->getBookingDetails($idBooking);
    $cargoAry = $kBooking->getCargoDeailsByBookingId($idBooking);
}
$post_data = $_POST['selectServiceAry'];

if(!empty($searchResultAry))
{ 	
	echo "SUCCESS|||| ";
	$final_ary=array();	
	$ctr=0;
	$iLanguage = getLanguageId();
	$countryKeyValueAry=array();	
	$countryKeyValueAry=$kConfig->getAllCountryInKeyValuePair(true,false,false,$iLanguage);
	if(!empty($post_data['fTotalPrice']))
	{
		$price_ary = explode(";",$post_data['fTotalPrice']);
		$min_price = $price_ary[0];
		$max_price = $price_ary[1];
	}		
	if(!empty($post_data['dtCutOffDate']))
	{
		$cutoff_ary = explode(";",$post_data['dtCutOffDate']);	
		$max_cutoff = strtotime(date('Y-m-d 23:59:59',($cutoff_ary[1])));
		$min_cutoff = strtotime(date('Y-m-d 00:00:00',($cutoff_ary[0]))) ;
	}		
	if(!empty($post_data['iTransitTime']))
	{
		$iTransitTime_ary = explode(";",$post_data['iTransitTime']);
		$min_iTransitTime = $iTransitTime_ary[0];
		$max_iTransitTime = $iTransitTime_ary[1];
	}
		
	if(!empty($post_data['iRatingReview']))
	{
		$iRatingReview_ary = explode(";",$post_data['iRatingReview']);
		$min_iRatingReview = (int)$iRatingReview_ary[0];
		$max_iRatingReview = (int)$iRatingReview_ary[1];
		if($min_iRatingReview == $max_iRatingReview)
		{
			//$min_iRatingReview =0;
		}
	}
	$minRatingToShowStars = $kWHSSearch->getManageMentVariableByDescription('__MINIMUM_RATING_TO_SHOW_RATING_STARS__');
	foreach($searchResultAry as $searchResultArys)
	{
		//echo "<br> forwarder id ".$searchResultArys['idForwarder'];
		
		// checking if the searched record is in the list of selected forwarder 
		if((!empty($post_data['idForwarder']) && in_array($searchResultArys['idForwarder'],$post_data['idForwarder'])))
		{			
		   //echo "<br> forwarder id ".$searchResultArys['idForwarder']." displaye price ceil ".ceil($searchResultArys['fDisplayPrice'])." display price floor ".floor($searchResultArys['fDisplayPrice'])." min price ".$min_price." max price  ".$max_price;
		   	// checking if record exist in between the price range and between the Transit hours range .
			if((floor($searchResultArys['fDisplayPrice']) >= $min_price) && (floor($searchResultArys['fDisplayPrice']) <= $max_price))
			{	
				//echo "<br> transit time ".($searchResultArys['iTransitHour']/24)." min transit ".$min_iTransitTime." max transit  ".$max_iTransitTime;
				if(((int)($searchResultArys['iTransitHour']/24)>=$min_iTransitTime) && ((int)($searchResultArys['iTransitHour']/24)<=$max_iTransitTime))
				{			
				  // echo " <br> date formated 3 ".date('Y-m-d',strtotime($searchResultArys['dtCuttOffDate_formated']))." min cuttoff  ".date('Y-m-d',$min_cutoff)." max cut off ".date('Y-m-d',$max_cutoff)." timing type ".$searchResultArys['idTimingType'];
					// if record is searched with idTimingType=1 (Ready at origin) then check record exists in range of dtCutOff or if idTimingType=2 ( Available at destination) then checks record is exists a in range of dtAvailableDate
					if(((strtotime(date('Y-m-d 00:00:00',strtotime($searchResultArys['dtCuttOffDate_formated'])))>=$min_cutoff) && ($searchResultArys['idTimingType']==1) && (strtotime(date('Y-m-d 00:00:00',strtotime($searchResultArys['dtCuttOffDate_formated'])))<=$max_cutoff) ) || ((strtotime($searchResultArys['dtAvailabeleDate_formated'])>=$min_cutoff) && (strtotime($searchResultArys['dtAvailabeleDate_formated'])<=$max_cutoff) && ($searchResultArys['idTimingType']==2)))
					{		
						/*	
						$szRatingVar = " <br>min rating to show star ".$minRatingToShowStars." min rating   ".$min_iRatingReview." max rating ".$max_iRatingReview." avg rating db ".$searchResultArys['fAvgRating']." numrating ".$searchResultArys['iNumRating']." floor val ".floor($searchResultArys['fAvgRating'])." ceil value ".ceil($searchResultArys['fAvgRating']);
						?>
						<script type="text/javascript">
							console.log('<?php echo $szRatingVar ?>');
						</script>		
						<?php
						*/
						if((($searchResultArys['fAvgRating']>=$min_iRatingReview) && ($searchResultArys['fAvgRating']<=$max_iRatingReview) && ($searchResultArys['iNumRating'] >= $minRatingToShowStars)) || (($searchResultArys['iNumRating'] < $minRatingToShowStars) && ($min_iRatingReview==0 || $max_iRatingReview==0)))
						{
							$final_ary[$ctr]=$searchResultArys ;						
						}	
						$ctr++;
					}
				}	
			}
		}
	}
	$searchResultAry = $final_ary ;

?>
	
	<?
		if($postSearchAry['idServiceType']== __SERVICE_TYPE_DTD__ )
		{
			display_dtd_services_list($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry);
		}
		else if(($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_DTW__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_DTP__) )
		{
			display_dtw_services_list($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry);
		}
		else if(($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_WTD__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_PTD__) )
		{
			display_wtd_services_list($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry);
		}
		else if(($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_WTW__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_WTP__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_PTP__) || ($postSearchAry['idServiceType']==(int)__SERVICE_TYPE_PTW__))
		{
			display_wtw_services_list($postSearchAry,$searchResultAry,$t_base,$countryKeyValueAry);
		}
	?>
	
	<input type="hidden" id="call_search_select_service" value="1">
	<input type="hidden" name="selectServiceAry[szBookingRandomNum]" value="<?=$szBookingRandomNum?>" id="service_szBookingRandomNum">
<?
die;
}
else
{
	echo "NO_RECORD_FOUND||||";
	$landing_page_url = __HOME_PAGE_URL__."/service_not_found/".$szBookingRandomNum.'/' ;
	?>
	<script type="text/javascript">
		var page_url = '<?=$landing_page_url?>'
		redirect_url(page_url);
	</script>
	<?
	die;
}
?>	