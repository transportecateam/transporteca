<?php
ob_start();
session_start();
$iGetLanguageFromRequest = 1;
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
include( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH__ . "/inc/functions.php" );
require_once( __APP_PATH__ . "/inc/I18n.php" );
require_once(__APP_PATH_LAYOUT__."/ajax_header.php");
require_once( __APP_PATH__ . "/inc/courier_functions.php" );
$kConfig=new cConfig();
$kBooking=new cBooking();
$kWHSSearch = new cWHSSearch();
$mode = sanitize_all_html_input(trim($_REQUEST['mode']));
$flag = sanitize_all_html_input(trim($_REQUEST['flag']));
 
if($flag=='DISPLAY_LEFT_MENU_HEADER')
{ 
    $kExplain = new cExplain;
    $howDoesworksLinks = array();
    $howDoesworksLinks = $kExplain->selectPageDetailsUserEnd($iLanguage,1);

    $companyLinksAry = array();
    $companyLinksAry = $kExplain->selectPageDetailsUserEnd($iLanguage,2);

    $tranportpediaLinksAry = array();
    $tranportpediaLinksAry = $kExplain->selectPageDetailsUserEnd($iLanguage,3);

    $seoPageHeaderAry = array();
    $seoPageHeaderAry = $kSEO->getAllPublishedSeoPages($iLanguage,true);

    $blogPageHeaderAry = array();
    $blogPageHeaderAry = $kExplain->selectBlogDetailsActive($iLanguage);
}
else if($mode=='GENERATE_SERVICE_LISTING')
{
    $szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_key']));
    $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum); 
    if($idBooking>0)
    {
        $t_base = "BookingDetails/";
        
        $postSearchAry = $kBooking->getBookingDetails($idBooking);   
        $searchResultAry = $kWHSSearch->get_search_result($postSearchAry,$cargoAry,false,$idBooking); 
        $postSearchAry['iNumRecordFound'] = count($searchResultAry); 
        if($postSearchAry['iNumRecordFound']>0)
        { 
            $iSearchResultType = $kWHSSearch->iSearchResultType; 

            $cheapestServiceAry = array();
            $fastestServiceAry = array();

            //Finding out cheapest service
            $searchResultArys = sortArray($searchResultAry,'fDisplayPrice',false,'iDaysBetween',false,false,false,true);
            $cheapestServiceAry = $searchResultArys[0];

            //Finding out fastest service
            $searchResultArys = sortArray($searchResultAry,'iAvailabeleDate_time',false,'fDisplayPrice',false,false,false,true);
            $fastestServiceAry = $searchResultArys[0];

            if($fastestServiceAry['idCourierProvider']<=0)
            {
               $fastestServiceAry['dtAvailableDate'] =  $fastestServiceAry['dtAvailabeleDate_formated'] ;
            }

            $iMonth = date('m',strtotime($fastestServiceAry['dtAvailableDate'])); 
            $szDeliveryDate = date('j.',strtotime($fastestServiceAry['dtAvailableDate'])); 
            $szMonthName = getMonthName($iLanguage,$iMonth);
            $szDeliveryDate .=" ".$szMonthName ;

            $fCheapestRate = $cheapestServiceAry['szCurrency']." ".getPriceByLang($cheapestServiceAry['fDisplayPrice'],$iLanguage);

            $iForwarderCount = 0;
            if($iSearchResultType==1) //Both LCL and Courier services available
            {
                if(!empty($searchResultAry))
                {
                    foreach($searchResultAry as $res_arys)
                    {							
                        if(!empty($forwarderAry) && in_array($res_arys['idForwarder'],$forwarderAry))
                        {
                            continue;
                        }
                        else
                        {
                            $forwarderAry[] = $res_arys['idForwarder'];
                            $iForwarderCount++;
                        }
                    }
                }
                $szFastestServiceText = t($t_base.'fields/fastest_can_deliver_on')." ".$szDeliveryDate;
                $szCheapestServiceText = t($t_base.'fields/cheapest_can_do_for')." ".$fCheapestRate;
                $szPageHeadingText = t($t_base.'fields/forwarders_ready_sharp_price');
            } 
            else if($iSearchResultType==2) //Only LCL services available
            {
                $szFastestServiceText = t($t_base.'fields/fastest_deliver_by_sea_on')." ".$szDeliveryDate;
                $szCheapestServiceText = t($t_base.'fields/cheapest_can_do_for')." ".$fCheapestRate;
                $szPageHeadingText = t($t_base.'fields/sharp_prices_from_selected_fwd');
                $iForwarderCount = count($searchResultAry); 
            }
            else if($iSearchResultType==3) //Only Courier services available
            {
                $szFastestServiceText = t($t_base.'fields/fastest_can_deliver_on')." ".$szDeliveryDate;
                $szCheapestServiceText = t($t_base.'fields/cheapest_airfreight_for')." ".$fCheapestRate;
                $szPageHeadingText = t($t_base.'fields/sharp_prices_from_selected_fwd');
                $iForwarderCount = count($searchResultAry); 
            }  
            $szServiceListStr = '<p class="fastest">'.$szFastestServiceText.'</p><p class="cheapest">'. $szCheapestServiceText.'</p>';
            echo "SUCCESS||||".$iForwarderCount."||||".$szServiceListStr."||||".$szPageHeadingText;
        }
    }
    die;
}
else if($flag=='DISPLAY_CC_ADDITIONAL_POPUP')
{
    $szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_key']));
    $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
    if($idBooking>0)
    {
        $postSearchAry = $kBooking->getBookingDetails($idBooking);
        $iOriginCC = sanitize_all_html_input(trim($_REQUEST['customClearanceAry']['iOriginCC']));
        $iDestinationCC = sanitize_all_html_input(trim($_REQUEST['customClearanceAry']['iDestinationCC']));

        if($iOriginCC==1 && $postSearchAry['idOriginCountry']==__ID_COUNTRY_CHINA__)
        {
            echo "SUCCESS||||";
            echo display_custom_clearance_addition_popup();
        }
    }
    die;
}
else if($mode == 'DISPLAY_CARGO_EXCEED_POPUP')
{
    $szBookingRandomNum = sanitize_all_html_input(trim($_REQUEST['booking_key'])); 
    $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);

    $postSearchAry = $kBooking->getBookingDetails($idBooking);
    echo "SUCCESS||||";
    echo display_weight_volume_exceed_popup($postSearchAry);
    die; 
}
else if($mode == 'SET_TRANSPORTECA_COOKIE')
{ 
    $cookieValue = "CookieValue";
    if(__ENVIRONMENT__ == "LIVE")
    {
        setcookie("__DISPLAY_COOKIE_NOTIFICATION__", $cookieValue, time()+(3600*24*90),'/',$_SERVER['HTTP_HOST'],true);
    }
    else
    {
        setcookie("__DISPLAY_COOKIE_NOTIFICATION__", $cookieValue, time()+(3600*24*90),'/');
    }
    unset($_SESSION['displayed_cookie_on_page']);
    
    $_SESSION['displayed_cookie_message']=0;
    unset($_SESSION['displayed_cookie_message']);
    echo "SUCCESS||||";
}
else if($mode == 'SAVE_WELCOME_USER_COOKIE')
{
    $value = md5('transporteca_'.time());	
    if(__ENVIRONMENT__ == "LIVE")
    {
            setcookie("__USER_FIRST_TIME_LOGIN_COOKIE", $value, time()+(3600*24*90),'/',$_SERVER['HTTP_HOST'],true);
    }
    else
    {
            setcookie("__USER_FIRST_TIME_LOGIN_COOKIE", $value, time()+(3600*24*90),'/');
    }
    die;
}
else if($mode =='CHANGE_SERVICE_TYPE_IMAGE')
{
    $idServiceType =sanitize_all_html_input(trim($_POST['id']));
    $kConfig = new cConfig();
    $auxServiceTypeAry=$kConfig->getConfigurationLanguageData('__TABLE_SERVICE_TYPE__');
    $alt_text = $auxServiceTypeAry[$idServiceType]['szDescription'];
    echo "SUCCESS||||";
    echo display_service_type_image_search_service($postSearchAry,$idServiceType,$alt_text);
}
else if(!empty($_POST['multiregionCityAry']))
{
    $res_ary=array();
    if((int)$_POST['multiregionCityAry']['szRegionFrom']>0)
    {
        $idFromPostcode = sanitize_all_html_input(trim($_POST['multiregionCityAry']['szRegionFrom'])) ;
        $kConfig->loadPostCode($idFromPostcode);

        $res_ary['idOriginPostCode'] = $kConfig->idPostCode;
        $res_ary['szOriginPostCode']= $kConfig->szPostCode;
        $res_ary['szOriginCity'] = $kConfig->szPostcodeCity;
        $res_ary['fOriginLatitude']= $kConfig->szPostcodeLatitude;
        $res_ary['fOriginLongitude'] = $kConfig->szPostcodeLongitude;		
    }
    if((int)$_POST['multiregionCityAry']['szRegionTo']>0)
    {
        $idFromPostcode = sanitize_all_html_input(trim($_POST['multiregionCityAry']['szRegionTo'])) ;
        $kConfig->loadPostCode($idFromPostcode);

        $res_ary['idDestinationPostCode'] = $kConfig->idPostCode;
        $res_ary['szDestinationPostCode']= $kConfig->szPostCode;
        $res_ary['szDestinationCity'] = $kConfig->szPostcodeCity;
        $res_ary['fDestinationLatitude']= $kConfig->szPostcodeLatitude;
        $res_ary['fDestinationLongitude'] = $kConfig->szPostcodeLongitude;		
    }

    $szBookingRandomNum = trim($_POST['multiregionCityAry']['szBookingRandomNum']);
    $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);

    //echo "<br> counter ".count($res_ary);
    if(!empty($res_ary))
    {
        foreach($res_ary as $key=>$value)
        {
            $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
        }
    }
    $update_query = rtrim($update_query,",");
    if($kBooking->updateDraftBooking($update_query,$idBooking))
    {
        echo "SUCCESS||||".$_SESSION['user_id']."||||".$szBookingRandomNum."||||".__SELECT_SERVICES_URL__."||||";
    }
}
else if(!empty($_POST['addExpactedServiceAry']))
{
    $kBooking = new cBooking();
    $landing_page_url = __LANDING_PAGE_URL__ ;
    $request_ary = array();
    $request_ary = $_POST['addExpactedServiceAry'];
    $idBooking = (int)$request_ary['idBooking'];
    $idExpactedService = trim($request_ary['idExpactedService']);
    $opration_type = trim($request_ary['type']);
    $szBookingRandomNumber = $kBooking->getBookingRandomNum($idBooking);
    $postSearchAry = $kBooking->getBookingDetails($idBooking);

    if(!empty($request_ary['szEmail']) && $request_ary['iSendNotification']==1)
    {
        if($kBooking->updateExpactedServices($request_ary))
        {
            if($opration_type=='BACK' || $opration_type=='NEW_SEARCH')
            {
                if($postSearchAry['iFromRequirementPage']==2 || $postSearchAry['iFromRequirementPage']==3)
                {
                    $landing_page_url = __NEW_SEARCH_PAGE_URL__ .'/'.$szBookingRandomNumber.'/' ;
                } 
                else
                {
                    $landing_page_url = __LANDING_PAGE_URL__.'/'.$szBookingRandomNumber.'/' ;
                }
            }
            else
            {
                $res_ary=array();
                $res_ary['idOriginCountry'] = 0;
                $res_ary['szOriginCountry'] = '';

                $res_ary['idDestinationCountry'] = 0;
                $res_ary['szDestinationCountry'] = '';

                $res_ary['szDestinationPostCode'] = '';
                $res_ary['szOriginPostCode'] = '';

                $res_ary['szOriginCity'] = '';
                $res_ary['szDestinationCity'] = '';

                $res_ary['fDestinationLatitude'] = '';
                $res_ary['fDestinationLongitude'] = '';

                $res_ary['fOriginLatitude'] = '';
                $res_ary['fOriginLongitude'] = '';

                $res_ary['idTimingType'] = '';
                $res_ary['dtTimingDate'] = '';
                $res_ary['iOriginCC'] = '';
                $res_ary['iDestinationCC'] = '';
                $res_ary['iCustomClearance'] = 0;

                $res_ary['idServiceType'] = '';
                if(!empty($res_ary))
                {
                    $update_query='';
                    foreach($res_ary as $key=>$value)
                    {
                        $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                    }
                }				
                $update_query = rtrim($update_query,",");

                $kBooking->updateDraftBooking($update_query,$idBooking); 

                if($postSearchAry['iFromRequirementPage']==2 || $postSearchAry['iFromRequirementPage']==3)
                {
                    $kConfig->loadCountry($postSearchAry['idOriginCountry']);
                    $szCountryStrUrl = $kConfig->szCountryISO ;
                    $kConfig->loadCountry($postSearchAry['idDestinationCountry']);
                    $szCountryStrUrl .= $kConfig->szCountryISO ; 

                    $landing_page_url =  __NEW_SERVICES_PAGE_URL__."/".$szBookingRandomNumber."/".__SERVICE_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;

                } 
                else
                {
                    $landing_page_url = __REQUIREMENT_PAGE_URL__.'/'.$szBookingRandomNumber.'/' ;
                } 
            }

            echo "SUCCESS_POPUP||||";
            echo display_d1_popup($landing_page_url,true,true);
            die;
        }
        else if(!empty($kBooking->arErrorMessages['szEmail']))
        {
            echo "ERROR||||";
            echo $kBooking->arErrorMessages['szEmail'];
            die;
        }
    }
    else
    {	
        if($opration_type=='BACK' || $opration_type=='NEW_SEARCH')
        {
            if($postSearchAry['iFromRequirementPage']==2)
            {
                $landing_page_url = __NEW_SEARCH_PAGE_URL__ .'/'.$szBookingRandomNumber.'/' ;
            } 
            else
            {
                $landing_page_url = __LANDING_PAGE_URL__.'/'.$szBookingRandomNumber.'/' ;
            }
            echo "SUCCESS||||" ;
            die;
        }
        else if($opration_type=='REQUIREMENT')
        {
            $res_ary=array();

            $res_ary['idOriginCountry'] = 0;
            $res_ary['szOriginCountry'] = '';

            $res_ary['idDestinationCountry'] = 0;
            $res_ary['szDestinationCountry'] = '';
            $res_ary['szDestinationPostCode'] = '';
            $res_ary['szOriginPostCode'] = '';

            $res_ary['szOriginCity'] = '';
            $res_ary['szDestinationCity'] = '';

            $res_ary['fDestinationLatitude'] = '';
            $res_ary['fDestinationLongitude'] = '';

            $res_ary['fOriginLatitude'] = '';
            $res_ary['fOriginLongitude'] = '';

            $res_ary['idTimingType'] = '';
            $res_ary['dtTimingDate'] = '';
            $res_ary['iOriginCC'] = '';
            $res_ary['iDestinationCC'] = '';
            $res_ary['iCustomClearance'] = 0;

            $res_ary['idServiceType'] = '';
            if(!empty($res_ary))
            {
                $update_query='';
                foreach($res_ary as $key=>$value)
                {
                    $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                }
            }				
            $update_query = rtrim($update_query,","); 
            $kBooking->updateDraftBooking($update_query,$idBooking);
            echo "SUCCESS||||" ;
            die;
        }
    }
}
else if(!empty($_POST['multiEmptyPostcodeCityAry']))
{
	$res_ary=array();
	
	$error_message = "";	
	if(((int)$_POST['multiEmptyPostcodeCityAry']['iRegionFrom']>0) && ((int)$_POST['multiEmptyPostcodeCityAry']['szRegionFrom']<=0))
	{
            $error_message .= "<li> Please select exact origin </li>";
	}
	if(((int)$_POST['multiEmptyPostcodeCityAry']['iRegionTo']>0) && ((int)$_POST['multiEmptyPostcodeCityAry']['szRegionTo']<=0))
	{
            $error_message .= "<li> Please select exact destination </li>";
	}
	if(!empty($error_message))
	{
            echo "ERROR||||".$error_message;
	}
	else
	{
		if((int)$_POST['multiEmptyPostcodeCityAry']['szRegionFrom']>0)
		{
                    $idFromPostcode = sanitize_all_html_input(trim($_POST['multiEmptyPostcodeCityAry']['szRegionFrom'])) ;
                    $kConfig->loadPostCode($idFromPostcode);

                    $res_ary['idOriginPostCode'] = $kConfig->idPostCode;
                    $res_ary['szOriginPostCode']= $kConfig->szPostCode;
                    $res_ary['szOriginCity'] = $kConfig->szPostcodeCity;
                    $res_ary['fOriginLatitude']= $kConfig->szPostcodeLatitude;
                    $res_ary['fOriginLongitude'] = $kConfig->szPostcodeLongitude;  
		}
		if((int)$_POST['multiEmptyPostcodeCityAry']['szRegionTo']>0)
		{
                    $idFromPostcode = sanitize_all_html_input(trim($_POST['multiEmptyPostcodeCityAry']['szRegionTo'])) ;
                    $kConfig->loadPostCode($idFromPostcode);

                    $res_ary['idDestinationPostCode'] = $kConfig->idPostCode;
                    $res_ary['szDestinationPostCode']= $kConfig->szPostCode;
                    $res_ary['szDestinationCity'] = $kConfig->szPostcodeCity;
                    $res_ary['fDestinationLatitude']= $kConfig->szPostcodeLatitude;
                    $res_ary['fDestinationLongitude'] = $kConfig->szPostcodeLongitude;
		}
		if(!empty($res_ary))
		{
                    foreach($res_ary as $key=>$value)
                    {
                        $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                    }
		}
		$szBookingRandomNum = trim($_POST['multiEmptyPostcodeCityAry']['szBookingRandomNum']);
		$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
		
		$update_query = rtrim($update_query,",");
		if($kBooking->updateDraftBooking($update_query,$idBooking))
		{
			echo "SUCCESS||||".$_SESSION['user_id']."||||".trim($szBookingRandomNum)."||||".__SELECT_SERVICES_URL__."||||";
		}
	}
}
else if(!empty($_POST['landingPageAry']))
{
	$kConfig=new cConfig();
	$data = $_POST['landingPageAry'] ;
	
	$kConfig->set_szOriginCountry($data['szOriginCountry']);
	$kConfig->set_szDestinationCountry($data['szDestinationCountry']);
	$t_base = "home/homepage/";
	if((($data['szOriginCountry']<=0) || ($data['szDestinationCountry']<=0)))
	{
		if($data['iCheck']==1)
		{
			//from index.php
			echo "ERROR||||".__REQUIREMENT_PAGE_URL__.'/' ;
			die;
		}
		else if($data['iCheck']==2 || $data['iCheck']==3)
		{
			//$landing_page_url = __HOME_PAGE_URL__."/home/service_not_found/".$szBookingRandomNum.'/';
			$landing_page_url = __REQUIREMENT_PAGE_URL__;
			echo "ERROR||||".$landing_page_url ;
			die;
		}
	}
	else if(!empty($kConfig->arErrorMessages))
	{
		echo "ERROR||||" ;
		?>
		<div id="popup-bg"></div>
			<div id="popup-container">
			 <div class="standard-popup popup" >
				<div id="popupRegError" class="errorBox1-requirement" style="display:block;">
					<div class="header"><?=t($t_base.'fields/please_correct_the_following');?></div>
					<div id="regErrorList">
					<ul>
					<?php
					      foreach($kConfig->arErrorMessages as $key=>$values)
					      {
					      ?><li><?=$values?></li>
					      <?php 
					      }
					?>
					</ul>
					</div>
				</div>
			<br/>
			<p align="center">
				<a href="javascript:void(0);" class="orange-button1" onclick="closePopup('error_popup');"><span><?=t($t_base.'fields/ok');?></span></a>
			</p>
		</div>
	</div>
		<?php
	}
	else
	{
		$postSearchAry['idOriginCountry'] = sanitize_all_html_input(trim($data['szOriginCountry']));
		$postSearchAry['idDestinationCountry'] = sanitize_all_html_input(trim($data['szDestinationCountry']));
		$postSearchAry['idLandingPage'] = sanitize_all_html_input(trim($data['idLandingPage']));
		
		$szBookingRandomNum = sanitize_all_html_input(trim($data['szBookingRandomNum']));
		if(!empty($szBookingRandomNum))
		{
			$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
		}
		
		$countryNameAry=array();
		$countryNameAry[0] = $postSearchAry['idOriginCountry'] ;
		$countryNameAry[1] = $postSearchAry['idDestinationCountry'];
		
		$iLanguage = getLanguageId();
		$orignCountrAry = $kConfig->getAllCountryInKeyValuePair(false,false,$countryNameAry,$iLanguage);
		
		$postSearchAry['szOriginCountry'] = $orignCountrAry[$postSearchAry['idOriginCountry']]['szCountryName'];
		$postSearchAry['szDestinationCountry'] = $orignCountrAry[$postSearchAry['idDestinationCountry']]['szCountryName'];

		
		if($idBooking>0)
		{
			$res_ary=array();
			$res_ary['idOriginCountry'] = $postSearchAry['idOriginCountry'] ;
			$res_ary['szOriginCountry'] = $postSearchAry['szOriginCountry'] ;
			$res_ary['szDestinationCountry'] = $postSearchAry['szDestinationCountry'] ;
			$res_ary['idDestinationCountry'] = $postSearchAry['idDestinationCountry'] ;
		
			if(!empty($res_ary))
			{
				foreach($res_ary as $key=>$value)
				{
					$update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
				}
			}
			$update_query = rtrim($update_query,",");
			if($kBooking->updateDraftBooking($update_query,$idBooking))
			{
				$compareBookingAry=array();
				$compareBookingAry['idOriginCountry'] = $postSearchAry['idOriginCountry'] ;
				$compareBookingAry['szOriginCountry'] = $postSearchAry['szOriginCountry'] ;
				$compareBookingAry['idDestinationCountry'] = $postSearchAry['idDestinationCountry'] ;
				$compareBookingAry['szDestinationCountry'] = $postSearchAry['szDestinationCountry'] ;
				$kBooking->addCompareBooking($compareBookingAry);
				
				if((int)$postSearchAry['idOriginCountry']>0 && (int)$postSearchAry['idDestinationCountry']>0)
				{
					$kWhsSearch = new cWHSSearch();
					$iAvailableServiceCount = $kWhsSearch->getAvailableService($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true);
									
					if($iAvailableServiceCount==0)
					{
						/*
						 * When we call this function from searchMini.php with iCheck=2 and there is nothing related to these countries in tblService then we redirect to landing page with "Sorry ..." popup
						 *
						 * **/
						if($data['iCheck']==2 || $data['iCheck']==3)
						{
							$landing_page_url = __HOME_PAGE_URL__."/home/service_not_found/".$szBookingRandomNum.'/';
							echo "ERROR||||".$landing_page_url ;
							die;
						}
						else
						{
							$kBooking->addExpactedServiceData($idBooking);
							$landing_page_url = __LANDING_PAGE_URL__ ;
							$idExpactedService = $kBooking->idExpactedService ;
							$t_base = "LandingPage/";
							echo "NO_SERVICE_POPUP||||";
							echo display_no_service_available_popup($t_base,$landing_page_url,$idBooking,$idExpactedService,$kBooking,$postSearchAry);
							die;
						}
					}
				}
				echo "SUCCESS||||".__REQUIREMENT_PAGE_URL__.'/'.$szBookingRandomNum.'/'; 
				die;
			}
			else
			{
				if((int)$postSearchAry['idOriginCountry']>0 && (int)$postSearchAry['idDestinationCountry']>0)
				{
					$kWhsSearch = new cWHSSearch();
					$iAvailableServiceCount = $kWhsSearch->getAvailableService($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true);
									
					if($iAvailableServiceCount==0)
					{
						/*
						 * When we call this function from searchMini.php with iCheck=2 and there is nothing related to these countries in tblService then we redirect to landing page with "Sorry ..." popup
						*
						* **/
						if($data['iCheck']==2 || $data['iCheck']==3)
						{
							$landing_page_url = __HOME_PAGE_URL__."/home/service_not_found/".$szBookingRandomNum.'/';
							echo "ERROR||||".$landing_page_url ;
							die;
						}
						else
						{
							$kBooking->addExpactedServiceData($idBooking);
							$landing_page_url = __LANDING_PAGE_URL__ ;
							$idExpactedService = $kBooking->idExpactedService ;
							$t_base = "LandingPage/";
							echo "NO_SERVICE_POPUP||||";
							echo display_no_service_available_popup($t_base,$landing_page_url,$idBooking,$idExpactedService,$kBooking,$postSearchAry);
							die;
						}
					}
				}
				echo "ERROR||||".__REQUIREMENT_PAGE_URL__.'/'.$szBookingRandomNum.'/';
				die;
			}
		}
		else
		{
			$szBookingRandomNum = md5(time());							
			$addBookingAry['idOriginCountry'] = $postSearchAry['idOriginCountry'] ;
			$addBookingAry['szOriginCountry'] = $postSearchAry['szOriginCountry'] ;
			$addBookingAry['szDestinationCountry'] = $postSearchAry['szDestinationCountry'] ;
			$addBookingAry['idDestinationCountry'] = $postSearchAry['idDestinationCountry'] ;
			$addBookingAry['szBookingRandomNum'] = $szBookingRandomNum;
			$addBookingAry['idLandingPage'] = $postSearchAry['idLandingPage'];
			
			if($kBooking->addBooking($addBookingAry))
			{
				$szBookingRandomNum = $kBooking->szBookingRandomNum ;
				$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
				
				if((int)$postSearchAry['idOriginCountry']>0 && (int)$postSearchAry['idDestinationCountry']>0)
				{
					$kWhsSearch = new cWHSSearch();
					$iAvailableServiceCount = $kWhsSearch->getAvailableService($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true);
									
					if($iAvailableServiceCount==0)
					{
						/*
						 * When we call this function from searchMini.php with iCheck=2 and there is nothing related to these countries in tblService then we redirect to landing page with "Sorry ..." popup
						*
						* **/
						if($data['iCheck']==2 || $data['iCheck']==3)
						{
							$landing_page_url = __HOME_PAGE_URL__."/home/service_not_found/".$szBookingRandomNum.'/';
							echo "ERROR||||".$landing_page_url ;
							die;
						}
						else
						{
							$kBooking->addExpactedServiceData($idBooking);
							$landing_page_url = __LANDING_PAGE_URL__ ;
							$idExpactedService = $kBooking->idExpactedService ;
							$t_base = "LandingPage/";
							echo "NO_SERVICE_POPUP||||";
							echo display_no_service_available_popup($t_base,$landing_page_url,$idBooking,$idExpactedService,$kBooking,$postSearchAry);
							die;
						}
					}
				}
				echo "SUCCESS||||".__REQUIREMENT_PAGE_URL__.'/'.$kBooking->szBookingRandomNum.'/'; 
				die;
			}
			else
			{
				if((int)$postSearchAry['idOriginCountry']>0 && (int)$postSearchAry['idDestinationCountry']>0)
				{
					$kWhsSearch = new cWHSSearch();
					$iAvailableServiceCount = $kWhsSearch->getAvailableService($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true);
									
					if($iAvailableServiceCount==0)
					{
						/*
						 * When we call this function from searchMini.php with iCheck=2 and there is nothing related to these countries in tblService then we redirect to landing page with "Sorry ..." popup
						*
						* **/
						if($data['iCheck']==2 || $data['iCheck']==3)
						{
							$landing_page_url = __HOME_PAGE_URL__."/home/service_not_found/".$szBookingRandomNum.'/';
							echo "ERROR||||".$landing_page_url ;
							die;
						}
						else
						{
							$kBooking->addExpactedServiceData($idBooking);
							$landing_page_url = __LANDING_PAGE_URL__ ;
							$idExpactedService = $kBooking->idExpactedService ;
							$t_base = "LandingPage/";
							echo "NO_SERVICE_POPUP||||";
							echo display_no_service_available_popup($t_base,$landing_page_url,$idBooking,$idExpactedService,$kBooking,$postSearchAry);
							die;
						}
					}
				}
				echo "ERROR||||".__REQUIREMENT_PAGE_URL__.'/' ;
				die;
			}
		}
	}
}
else
{ 
    $postSearchAry=$_REQUEST['searchAry'];
    $cargodetailAry = $_REQUEST['cargodetailAry'];
    $iCalculateOnlyCourier = $_REQUEST['searchAry']['iCalculateOnlyCourier']; 
    $iPageVersion = $_REQUEST['searchAry']['iPageVersion']; 
    
    $iSearchMiniPage = (int)sanitize_all_html_input($_REQUEST['searchAry']['iSearchMiniPage']);
    
    $szFormId = sanitize_all_html_input($_REQUEST['formId']);
    $szPageUrl = sanitize_all_html_input($_REQUEST['page_url']);
    $iHiddenFlag = sanitize_all_html_input($_REQUEST['hidden_flag']);
    $iSearchMiniVersion = sanitize_all_html_input($_REQUEST['search_mini']);
    $szFromPage = sanitize_all_html_input($_REQUEST['from_page']);
    $iDonotShowSearchNotificationPopup = sanitize_all_html_input($_REQUEST['iDonotShowSearchNotificationPopup']); 
    $iConvertRfq = sanitize_all_html_input($_REQUEST['iConvertRfq']); 
    $iOfferType = sanitize_all_html_input($_REQUEST['iOfferType']); 
     
    $additionalAry = array();
    $additionalAry['szFormId'] = $szFormId;
    $additionalAry['szPageUrl'] = $szPageUrl;
    $additionalAry['iHiddenFlag'] = $iHiddenFlag;
    $additionalAry['iSearchMiniVersion'] = $iSearchMiniVersion;
    $additionalAry['szFromPage'] = $szFromPage;
    
    $iSearchMiniPageVersion = 0;
    if($iSearchMiniPage>2)
    {
        $iSearchMiniPageVersion = $iSearchMiniPage;
    }
    
    $postSearchAry['iVolume']=str_replace(",",".",$postSearchAry['iVolume']);
    if($postSearchAry['szPageName']=='SIMPLEPAGE' || $postSearchAry['szPageName']=='NEW_LANDING_PAGE')
    {
        $kConfig=new cConfig();
        if(!empty($_COOKIE['FROM_SEARCH_COOKIE_KEY']))
        {
            $szCountryStrKey_old = $_COOKIE['FROM_SEARCH_COOKIE_KEY']; 
            if(__ENVIRONMENT__ == "LIVE")
            {
                setcookie('FROM_SEARCH_COOKIE_KEY', '', time()-(3600*24*30),'/',$_SERVER['HTTP_HOST'],true); 
                setcookie($szCountryStrKey_old, '', time()-(3600*24*30),'/',$_SERVER['HTTP_HOST'],true);
            }
            else
            {
                setcookie('FROM_SEARCH_COOKIE_KEY', '', time()-(3600*24*30),'/',$_SERVER['HTTP_HOST'],true); 
                setcookie($szCountryStrKey_old, '', time()-(3600*24*30),'/',$_SERVER['HTTP_HOST'],true);
            }
        }
        
        if(!empty($_COOKIE['TO_SEARCH_COOKIE_KEY']))
        {
            $szCountryStrKey_old = $_COOKIE['TO_SEARCH_COOKIE_KEY']; 
            if(__ENVIRONMENT__ == "LIVE")
            {
                setcookie('TO_SEARCH_COOKIE_KEY', '', time()-(3600*24*30),'/',$_SERVER['HTTP_HOST'],true); 
                setcookie($szCountryStrKey_old, '', time()-(3600*24*30),'/',$_SERVER['HTTP_HOST'],true);
            }
            else
            {
                setcookie('TO_SEARCH_COOKIE_KEY', '', time()-(3600*24*30),'/',$_SERVER['HTTP_HOST'],true); 
                setcookie($szCountryStrKey_old, '', time()-(3600*24*30),'/',$_SERVER['HTTP_HOST'],true);
            }
        }
            
        if($postSearchAry['szOriginCountryStr']!='')
        {
            $szOriginCountryArr = reverse_geocode($postSearchAry['szOriginCountryStr'],true);  
            $replaceAry = array(" ",",");
            $szCountryStrKey = str_replace($replaceAry, "-", $postSearchAry['szOriginCountryStr']); 
            $szCountryStrSerialized = serialize($szOriginCountryArr);   
            
            if(__ENVIRONMENT__ == "LIVE")
            {
                setcookie('FROM_SEARCH_COOKIE_KEY', $szCountryStrKey, time()+(3600),'/',$_SERVER['HTTP_HOST'],true); 
                setcookie($szCountryStrKey, $szCountryStrSerialized, time()+(3600),'/',$_SERVER['HTTP_HOST'],true);
            }
            else
            {
                setcookie('FROM_SEARCH_COOKIE_KEY', $szCountryStrKey, time()+(3600),'/');  
                setcookie($szCountryStrKey, $szCountryStrSerialized, time()+(3600),'/');
            } 
            $postSearchAry['szOriginCountry']=$szOriginCountryArr['szCountryName'];
            $idOriginCountry=$kConfig->getCountryIdByCountryName($szOriginCountryArr['szCountryName'],$iLanguage);

            if($idOriginCountry<=0 && !empty($szOriginCountryArr['szCountryCode']))
            {
                $kConfig->loadCountry(false,$szOriginCountryArr['szCountryCode']);
                $idOriginCountry = $kConfig->idCountry ; 
            }

            $postSearchAry['szOriginCountry']=$idOriginCountry;
            $postSearchAry['szOLat']=$szOriginCountryArr['szLat'];
            $postSearchAry['szOLng']=$szOriginCountryArr['szLng'];
            $postSearchAry['szOriginCity']=$szOriginCountryArr['szCityName']; 
            $postSearchAry['szOriginPostCode'] = $szOriginCountryArr['szPostCode']; 
            $postSearchAry['szOriginPostCodeTemp'] = $szOriginCountryArr['szPostcodeTemp']; 
            
            $postSearchAry['iOriginPostcodeFoundAtStep'] = 1;

            if(empty($postSearchAry['szOriginPostCode']))
            {
                $postcodeCheckAry=array();
                $postcodeResultAry = array();

                $postcodeCheckAry['idCountry'] = $idOriginCountry ;
                $postcodeCheckAry['szLatitute'] = $szOriginCountryArr['szLat'] ;
                $postcodeCheckAry['szLongitute'] = $szOriginCountryArr['szLng'] ;

                $postcodeResultAry = $kConfig->getPostcodeFromGoogleCordinate($postcodeCheckAry); 
                if(!empty($postcodeResultAry))
                {
                    $postSearchAry['szOriginPostCode'] = $postcodeResultAry['szPostCode'];
                    $postSearchAry['iOriginPostcodeFoundAtStep'] = 2;
                    $postSearchAry['szOriginPostCodeTemp'] = $postcodeResultAry['szPostCode']; 
                }
            } 
        }  
        if($postSearchAry['szDestinationCountryStr']!='')
        {
            $szDesCountryArr=reverse_geocode($postSearchAry['szDestinationCountryStr'],true); 
            
            $replaceAry = array(" ",",");
            $szCountryStrKey = str_replace($replaceAry, "-", $postSearchAry['szDestinationCountryStr']); 
            $szCountryStrSerialized = serialize($szDesCountryArr);   
            
            if(__ENVIRONMENT__ == "LIVE")
            {
                setcookie('TO_SEARCH_COOKIE_KEY', $szCountryStrKey, time()+(3600),'/',$_SERVER['HTTP_HOST'],true); 
                setcookie($szCountryStrKey, $szCountryStrSerialized, time()+(3600),'/',$_SERVER['HTTP_HOST'],true);
            }
            else
            {
                setcookie('TO_SEARCH_COOKIE_KEY', $szCountryStrKey, time()+(3600),'/');  
                setcookie($szCountryStrKey, $szCountryStrSerialized, time()+(3600),'/');
            }
            $postSearchAry['szDestinationCountry']=$szDesCountryArr['szCountryName'];
            $idDesCountry=$kConfig->getCountryIdByCountryName($szDesCountryArr['szCountryName'],$iLanguage);

            if($idDesCountry<=0 && !empty($szDesCountryArr['szCountryCode']))
            {
                $kConfig->loadCountry(false,$szDesCountryArr['szCountryCode']);
                $idDesCountry = $kConfig->idCountry ; 
            }

            $postSearchAry['szDestinationCountry']=$idDesCountry;
            $postSearchAry['szDLat']=$szDesCountryArr['szLat'];
            $postSearchAry['szDLng']=$szDesCountryArr['szLng'];
            $postSearchAry['szDestinationCity']=$szDesCountryArr['szCityName'];
            $postSearchAry['szDestinationPostCode'] = $szDesCountryArr['szPostCode']; 
            $postSearchAry['iDestinationPostcodeFoundAtStep'] = 1;
            $postSearchAry['szDestinationPostCodeTemp'] = $szDesCountryArr['szPostcodeTemp'];

            if(empty($postSearchAry['szDestinationPostCode']))
            {
                $postcodeCheckAry = array(); 
                $postcodeResultAry = array(); 
                $postcodeCheckAry['idCountry'] = $idDesCountry ;
                $postcodeCheckAry['szLatitute'] = $szDesCountryArr['szLat'] ;
                $postcodeCheckAry['szLongitute'] = $szDesCountryArr['szLng'] ;
                $postcodeResultAry = $kConfig->getPostcodeFromGoogleCordinate($postcodeCheckAry);

                if(!empty($postcodeResultAry))
                {
                    $postSearchAry['szDestinationPostCode'] = $postcodeResultAry['szPostCode'];
                    $postSearchAry['szDestinationPostCodeTemp'] = $postcodeResultAry['szPostCode'];
                    $postSearchAry['iDestinationPostcodeFoundAtStep'] = 2;
                }
            } 
        } 
        $iShipperConsigneeFlag = false;
        if((int)$_SESSION['user_id']>0)
        {
            $kUser = new cUser();
            $kUser->getUserDetails($_SESSION['user_id']); 
            $idCustomerCountry = $kUser->szCountry;  
        }
        else
        { 
            $userIpDetailsAry = array();
            $userIpDetailsAry = getCountryCodeByIPAddress(); 
            $szUserCountryName  = $userIpDetailsAry['szCountryCode'] ; 
                  
            if(!empty($szUserCountryName))
            {
                $kConfigCountry = new cConfig();
                $kConfigCountry->loadCountry(false,$szUserCountryName);
                
                $idCustomerCountry = $kConfigCountry->idCountry;
            } 
        }
        
        /*
        * If customer country = Origin country the we searches for service type: DTP
        */
        if($idCustomerCountry == $idOriginCountry)
        {
            $iShipperConsigneeFlag = 1;
        }
        else if($idCustomerCountry == $idDestinationCountry)
        {
            $iShipperConsigneeFlag = 1;
        }
        else
        {
            $iShipperConsigneeFlag = 3;
        } 
        if($iPageVersion==2)
        {
            $kBooking_new = new cBooking();
            $szBookingRandomNum = $kBooking_new->getUniqueBookingKey(10);
            $szBookingRandomNum = $kBooking_new->isBookingRandomNumExist($szBookingRandomNum);
            $postSearchAry['szBookingRandomNum'] = $szBookingRandomNum ;
        }
        
        $postSearchAry['iBookingStep']=1;
        $postSearchAry['iWeight']=$postSearchAry['iWeight'];
        $postSearchAry['iQuantity'][1]='1';
        $postSearchAry['idWeightMeasure'][1]='1';

        if($postSearchAry['szPageName']=='SIMPLEPAGE')
        {
            $postSearchAry['iFromRequirementPage']=2;
        }
        else
        {
            $postSearchAry['iFromRequirementPage']=2;
        } 
        
        if($postSearchAry['idServiceType']==__SERVICE_TYPE_DTD__)
        {
            $postSearchAry['idCC'][0] = '1';
            $postSearchAry['idCC'][1]='2';
        }
        else if($iShipperConsigneeFlag==1) //DTP with Origin custom clearance
        {
            $postSearchAry['idCC'][0] = '1';
            $postSearchAry['idServiceType'] = __SERVICE_TYPE_DTP__;
        } 
        else
        {
            $postSearchAry['idServiceType'] = __SERVICE_TYPE_PTD__; 
            $postSearchAry['idCC'][1]='2';
        }
    }  
    $t_base = "home/homepage/";  
    $searchFormAry = array();
    $searchFormAry = $postSearchAry ; 
    if($postSearchAry['szPageName']=='NEW_LANDING_PAGE')
    {
        $searchFormAry['szPageName'] = 'SIMPLEPAGE';
    }
    $iCalculateVogaAutomaticService = false; 
    if($iSearchMiniPageVersion==7 || $iSearchMiniPageVersion==8 || $iSearchMiniPageVersion==9) 
    { 
        $kConfig->validateVogueLandingPage($searchFormAry,$cargodetailAry,$iSearchMiniPageVersion);  
         
        if($kConfig->iVogaSearchAutomatic)
        {
            //$iCalculateVogaAutomaticService = true;  
        }
    }
    else
    { 
        $kConfig->validateLandingPage($searchFormAry,$cargodetailAry,$iSearchMiniPageVersion);  
    } 
     
    if(!empty($kConfig->arErrorMessages))
    {
        if($postSearchAry['szPageName']=='NEW_LANDING_PAGE')
        {
            echo "ERROR||||";			
            $hidden_search_form=false;	
            $id_suffix = "";		
            if($iSearchMiniPageVersion>0)
            {
                $id_suffix = "_V_".$iSearchMiniPageVersion;
            }
            else if($postSearchAry['iHiddenValue']==1)
            {
                $id_suffix = "_hiden";
            }
            $szErrorMessageStr = '';
            foreach($kConfig->arErrorMessages as $errorKey=>$errorValue)
            {
                if($errorKey=='szOriginCountry')
                {
                    $errorKey = 'szOriginCountryStr'.$id_suffix.'_container+++++'.$errorValue.'+++++szOriginCountryStr'.$id_suffix;
                } 
                else if($errorKey=='szDestinationCountry')
                {
                    $errorKey = 'szDestinationCountryStr'.$id_suffix.'_container+++++'.$errorValue;
                } 
                else if($errorKey=='szDestinationCountry')
                {
                    $errorKey = 'szDestinationCountryStr'.$id_suffix.'_container+++++'.$errorValue;
                }
                else if($errorKey=='fCargoWeight_landing_page')
                {
                    $errorKey = 'iWeight'.$id_suffix.'_container+++++'.$errorValue;
                }
                else if($errorKey=='iVolume')
                {
                    $errorKey = 'iVolume'.$id_suffix.'_container+++++'.$errorValue;
                }
                else if($errorKey=='iNumColli')
                {
                    $errorKey = 'iNumColli'.$id_suffix.'_container+++++NO_MESSAGE_TO_SHOW';
                } 
                else if($errorKey=='iNumColli')
                {
                    $errorKey = 'iNumColli'.$id_suffix.'_container+++++NO_MESSAGE_TO_SHOW';
                }
                else if($errorKey=='iPalletType')
                {
                    $errorKey = 'iPalletType'.$id_suffix.'_container+++++NO_MESSAGE_TO_SHOW';
                }
                else if($errorKey=='szFirstName')
                {
                    $errorKey = 'szFirstName'.$id_suffix.'_container+++++NO_MESSAGE_TO_SHOW';
                }
                else if($errorKey=='szLastName')
                {
                    $errorKey = 'szLastName'.$id_suffix.'_container+++++NO_MESSAGE_TO_SHOW';
                } 
                else if($errorKey=='szEmail')
                {
                    $errorKey = 'szEmail'.$id_suffix.'_container+++++NO_MESSAGE_TO_SHOW';
                }
                else if($errorKey=='szPhone')
                {
                    $errorKey = 'szPhone'.$id_suffix.'_container+++++NO_MESSAGE_TO_SHOW';
                }
                else if($errorKey=='szAddress')
                {
                    $errorKey = 'szAddress'.$id_suffix.'_container+++++NO_MESSAGE_TO_SHOW';
                }
                else if($errorKey=='szPostcode')
                {
                    $errorKey = 'szPostcode'.$id_suffix.'_container+++++NO_MESSAGE_TO_SHOW';
                }
                else if($errorKey=='szCity')
                {
                    $errorKey = 'szCity'.$id_suffix.'_container+++++NO_MESSAGE_TO_SHOW';
                }
                else if($errorKey=='szConsigneeFirstName')
                {
                    $errorKey = 'szConsigneeFirstName'.$id_suffix.'_container+++++NO_MESSAGE_TO_SHOW';
                }
                else if($errorKey=='szConsigneeLastName')
                {
                    $errorKey = 'szConsigneeLastName'.$id_suffix.'_container+++++NO_MESSAGE_TO_SHOW';
                }
                else if($errorKey=='szConsigneeEmail')
                {
                    $errorKey = 'szConsigneeEmail'.$id_suffix.'_container+++++NO_MESSAGE_TO_SHOW';
                }
                else if($errorKey=='szConsigneePhone')
                {
                    $errorKey = 'szConsigneePhone'.$id_suffix.'_container+++++NO_MESSAGE_TO_SHOW';
                }
                else if($errorKey=='idConsigneeDialCode')
                {
                    $errorKey = 'idConsigneeDialCode'.$id_suffix.'_container+++++NO_MESSAGE_TO_SHOW';
                }
                else if($errorKey=='szConsigneeAddress')
                {
                    $errorKey = 'szConsigneeAddress'.$id_suffix.'_container+++++NO_MESSAGE_TO_SHOW';
                }
                else if($errorKey=='szConsigneePostcode')
                {
                    $errorKey = 'szConsigneePostcode'.$id_suffix.'_container+++++NO_MESSAGE_TO_SHOW';
                }
                else if($errorKey=='szConsigneeCity')
                {
                    $errorKey = 'szConsigneeCity'.$id_suffix.'_container+++++NO_MESSAGE_TO_SHOW';
                }
                else if($errorKey=='idConsigneeCountry')
                {
                    $errorKey = 'idConsigneeCountry'.$id_suffix.'_container+++++NO_MESSAGE_TO_SHOW';
                }
                else if($errorKey=='iContactShipper')
                {
                    $errorKey = 'custome_cb_iContactShipper'.$id_suffix.'+++++NO_MESSAGE_TO_SHOW';
                } 
                else if($errorKey=='dtTiming')
                {
                    $errorKey = 'dtTiming'.$id_suffix.'_container+++++'.$errorValue;
                }
                else
                {
                    $errorKey = $errorKey.'+++++NO_MESSAGE_TO_SHOW';
                }
                $szErrorMessageStr .= $errorKey."$$$$";
            } 
            echo $szErrorMessageStr ; 
            die;
        }
        else
        {
            echo "ERROR||||" ;
            ?>
            <div class="header"><?=t($t_base.'fields/please_correct_the_following');?></div>
            <div id="regErrorList">
                <ul>
                    <?php
                        foreach($kConfig->arErrorMessages as $key=>$values)
                        {
                            ?><li><?=$values?></li><?php 
                        }
                    ?>
                </ul>
            </div>
            <?php
        } 
    }
    else if($postSearchAry['szPageName']=='SIMPLEPAGE' || $postSearchAry['szPageName']=='NEW_LANDING_PAGE')
    {    
        $showNotificationPopup=true;
        if($iSearchMiniPageVersion==8 || $iSearchMiniPageVersion==9)
        {
            $postSearchAry['iDoNotShowPrivatePopForVogaPage']=true;
        }        
        if($iSearchMiniPageVersion==7)
        {
            $showNotificationPopup=false;
        }
        if($showNotificationPopup && $iDonotShowSearchNotificationPopup!=1)
        { 
            $searchNotificationPostSearchAry = array();
            $searchNotificationPostSearchAry = $postSearchAry;
            $searchNotificationPostSearchAry['idOriginCountry'] = $postSearchAry['szOriginCountry'];
            $searchNotificationPostSearchAry['idDestinationCountry'] = $postSearchAry['szDestinationCountry'];
            $searchNotificationPostSearchAry['szOriginCountry'] = $postSearchAry['szOriginCountryStr'];
            $searchNotificationPostSearchAry['szDestinationCountry'] = $postSearchAry['szDestinationCountryStr'];
            
            $kExplain = new cExplain();
            $searchNotificationArr = array();
            $searchNotificationArr = $kExplain->isSearchNotificationExists($searchNotificationPostSearchAry);

            if(!empty($searchNotificationArr))
            { 
                echo "SUCCESS_LOCAL_SEARCH||||";
                echo display_local_search_popup_new($searchNotificationArr,$idBooking,$szCountryStrUrl,$iNumRecordFound,'','',$iSearchMiniPageVersion,$additionalAry);
                die;
            } 
        }  
        $kBooking=new cBooking();
        $kCourierServices = new cCourierServices();
        $szBookingRandomNum = $kConfig->szBookingRandomNum;
        $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);

        $szReuquestPageUrl = __MAIN_SITE_HOME_PAGE_SECURE_URL__."".$postSearchAry['szReuquestPageUrl'];

        $kConfig_new = new cConfig(); 
        $countryAry = array();
        $searchCountryAry = array();
        
        $iLanguage = getLanguageId();

        $countryAry = array($kConfig->szOriginCountry,$kConfig->szDestinationCountry);
        $searchCountryAry = $kConfig_new->getAllCountryInKeyValuePair(false,false,$countryAry,$iLanguage); 

        $szCountryStrUrl = $searchCountryAry[$kConfig->szOriginCountry]['szCountryISO'];
        $iAVailableFromDropDown = $searchCountryAry[$kConfig->szOriginCountry]['iActiveFromDropdown'];

        $szCountryStrUrl .= $searchCountryAry[$kConfig->szDestinationCountry]['szCountryISO'];
        $iAVailableToDropDown = $searchCountryAry[$kConfig->szDestinationCountry]['iActiveFromDropdown'];

        $idDefaultLandingPage = $postSearchAry['idDefaultLandingPage'];
        $kConfig->idDefaultLandingPage = $idDefaultLandingPage ;

        $kConfig->szPageSearchType = 'SIMPLEPAGE';
        $iPalletType = $kConfig->iPalletType;
        if($idBooking>0)
        {
            $kBooking->save($kConfig,$idBooking,$szReuquestPageUrl);
        }
        else
        {
            $kBooking->add($kConfig,__BOOKING_STATUS_ZERO_LEVEL__,$szReuquestPageUrl);
            $idBooking = $kBooking->idBooking;
        }     
        $bDonotCalculateServices = false;
        if($iConvertRfq==1)
        {
            /*
            * If user selects 'Conert to RFQ' button from search notification popup then we don't make search just creates a Validate request in Pending Tray
            */
            $bDonotCalculateServices = true;
        }
        else if(($iSearchMiniPageVersion==7 || $iSearchMiniPageVersion==8 || $iSearchMiniPageVersion==9) && !$iCalculateVogaAutomaticService)
        {
            $bDonotCalculateServices = true;
        }
        else if(($iSearchMiniPageVersion==5 || $iSearchMiniPageVersion==6) && ($iPalletType==3))
        {
            $bDonotCalculateServices = true;
        } 
        $idSeoPage = $postSearchAry['idSeoPage'] ; 
        if($idSeoPage>0)
        {
            $kSEO = new cSEO();
            $seoAquisitionLinkAry = array();
            $seoAquisitionLinkAry = $kSEO->getSeoPageDataByUrl(false,false,false,$idSeoPage); 

            $seoTrackerAry = array();
            $seoTrackerAry['szReferalUrl'] = $szReuquestPageUrl;
            $seoTrackerAry['szSeoUrl'] = $seoAquisitionLinkAry['szUrl'];
            $seoTrackerAry['idSeoPage'] = $seoAquisitionLinkAry['id']; 
            $kSEO->addSEOTracker($seoTrackerAry);

            unset($_SESSION['seo_aquisition']['szReferalUrl']);
            unset($_SESSION['seo_aquisition']['szUrl']);
        } 
        $kConfig=new cConfig();
        $kWHSSearch=new cWHSSearch();

        $iLanguage = getLanguageId();
        $searchResult=array(); 
        $kBooking = new cBooking();
        $searchResultAry=array();
        $_SESSION['do-not-load-booking-details'] = 1 ;

        //$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
        $iNumRecordFound = 0;
        if($idBooking>0)
        { 
            $postSearchAry = $kBooking->getBookingDetails($idBooking); 
            
             
            /*
            * $iSearchMiniPageVersion = 7 means, Voga: Standard Cargo – Delivery Address.
            * $iSearchMiniPageVersion = 8 means, Voga: Standard Cargo – From and To. Removed 03-jan-2017
            
            if($iSearchMiniPageVersion!=7 && $iSearchMiniPageVersion!=8)
            {
                $kExplain = new cExplain();
                $searchNotificationArr = array();
                $searchNotificationArr = $kExplain->isSearchNotificationExists($postSearchAry);
                   
                if(!empty($searchNotificationArr))
                { 
                    echo "SUCCESS_LOCAL_SEARCH||||";
                    echo display_local_search_popup_new($searchNotificationArr,$idBooking,$szCountryStrUrl,$iNumRecordFound,'','',$iSearchMiniPageVersion);
                    die;
                } 
            }   
            */
            /*
            $iPageVersion = 2;
            if($_SESSION['user_id']<=0 || $iPageVersion==2)
            { 
                $iNumRecordFound = $kWHSSearch->getAvailableService($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true); 
                if($iNumRecordFound<=0)
                {
                    $searchResultAry = $kWHSSearch->getCourierProviderData($postSearchAry,$idCustomerCurrencye);
                    $iNumRecordFound = count($searchResultAry);
                }
                $postSearchAry['iNumRecordFound'] = $iNumRecordFound; 
            }
            else
            { 
                $searchResultAry = $kWHSSearch->get_search_result($postSearchAry,$cargoAry,false,$idBooking); 
                $postSearchAry['iNumRecordFound'] = count($searchResultAry);
                $iNumRecordFound = count($searchResultAry);
            }  
            */
            if(!$bDonotCalculateServices)
            {
                if($iCalculateVogaAutomaticService)
                {
                    $searchResultAry = $kWHSSearch->get_search_result($postSearchAry,$cargoAry,false,$idBooking); 
                    $postSearchAry['iNumRecordFound'] = count($searchResultAry);
                    $iNumRecordFound = count($searchResultAry);
                    
                    $postSearchAry['iNumRecordFound'] = $iNumRecordFound; 
                    $kBooking->addSelectServiceData($postSearchAry);
                        
                    if($iNumRecordFound>0)
                    {
                        /*
                        *  Creating Automatic quotes for Voga pages and added this to Mgt >  Pending Tray > Quote
                        */  
                        $kConfig->createQuoteForVogaBooking($postSearchAry,$searchResultAry,$idDefaultLandingPage); 
                        $iNumRecordFound = 0;
                    }
                } 
                else
                {
                    $iNumRecordFound = $kWHSSearch->getAvailableService($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true);  
                    if($iNumRecordFound<=0)
                    {
                        $searchResultAry = $kWHSSearch->get_search_result($postSearchAry,$cargoAry,false,$idBooking); 
                        $postSearchAry['iNumRecordFound'] = count($searchResultAry);
                        $iNumRecordFound = count($searchResultAry);
                        
                        $postSearchAry['iNumRecordFound'] = $iNumRecordFound; 
                        $kBooking->addSelectServiceData($postSearchAry); 
                    }
                }  
            }
            
            /*
            * Registering file search log in Pending Tray > File Log
            */
            $kBooking->insertBookingSearchLogs($postSearchAry); 
            
            $iSmallShipmentFlag = $postSearchAry['iSmallShipment'];
            
            $_SESSION['came_from_landing_page'.'_'.$szBookingRandomNum]=0;
            unset($_SESSION['came_from_landing_page']);

            // initialising arrays
            $countryAry=array();
            $serviceTypeAry=array();
            $weightMeasureAry=array();
            $cargoMeasureAry = array();
            $timingTypeAry=array();
            $forwarderAry=array();
        }
        else
        {
            $postSearchAry = array();
            $cargoAry = array();
        }  
        //$postSearchAry = $kBooking->getBookingDetails($idBooking); 
        $szBookingRandomNum = $postSearchAry['szBookingRandomNum'] ;
        $postSearchAry['iNumRecordFound']=$iNumRecordFound;
        if((int)$iNumRecordFound>0 && $_SESSION['user_id']<=0)
        {
            $res_ary=array(); 
            $res_ary['iBookingStep'] = 3 ; 
            $res_ary['iBookingQuotes'] = 0 ;

            if(!empty($res_ary))
            {
                $update_query = '';
                foreach($res_ary as $key=>$value)
                {
                    $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                }
            } 
            $update_query = rtrim($update_query,",");
            if($kBooking->updateDraftBooking($update_query,$idBooking))
            {
                //booking moves on to second step means Trade Available.
            } 
        }
        else
        { 
            $res_ary=array();
            if((int)$postSearchAry['iBookingStep']<7)
            {
                $res_ary['iBookingStep'] = 7 ;
            } 
            $res_ary['iBookingQuotes'] = 1 ;
            if((int)$iNumRecordFound<=0)
            {
                $res_ary['iGetQuotePageFlag'] = 1 ;
            }
            if(!empty($res_ary))
            {
                $update_query = '';
                foreach($res_ary as $key=>$value)
                {
                    $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                }
            } 
            $update_query = rtrim($update_query,",");
            if($kBooking->updateDraftBooking($update_query,$idBooking))
            {
                //booking moves on to second step means Trade Available.
            } 
        }   
        echo "REDIRECT||||";    
        if($iNumRecordFound>0)
        {
            /*
            *  IF small shipment flag is set then we send user directly to /service/ page even if iser is not logged in
            */ 
            if($_SESSION['user_id']>0 || $iSmallShipmentFlag==1)
            { 
                echo __NEW_SERVICES_PAGE_URL__."/".$szBookingRandomNum."/".__SERVICE_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
                die;
            }
            else
            {
                if($iCalculateOnlyCourier==1)
                {
                    echo __BOOKING_TEST_GET_RATES_PAGE_URL__."/".$szBookingRandomNum."/".__QUOTATION_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
                    die;
                } 
                else if($iPageVersion==3)
                {
                    echo __BOOKING_GET_RATES_PAGE_URL__."/".$szBookingRandomNum."/".__QUOTATION_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
                    die;
                }
                else
                {
                    echo __BOOKING_GET_RATES_PAGE_URL__."/".$szBookingRandomNum."/".__QUOTATION_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
                    die;
                } 
            } 
        }
        else
        { 
            if($iSearchMiniPageVersion==7 || $iSearchMiniPageVersion==8 || $iSearchMiniPageVersion==9)
            {  
                echo __BOOKING_QUOTE_THANK_YOU_PAGE_URL__."/".___BOOKING_QUOTE_THANK_YOU_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
                die;
            }
            else if($iConvertRfq==1)
            {
                /*
                * Converting booking to RFQ
                */
                $kQuote = new cQuote();
                if($kQuote->convertBookingtoRFQ($postSearchAry,$iOfferType,$szFromPage))
                {  
                    $redirect_url = __BOOKING_QUOTE_THANK_YOU_PAGE_URL__."/".___BOOKING_QUOTE_THANK_YOU_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ; 
                    echo $redirect_url;
                    die; 
                }
            }
            else
            {
                $kWHSSearch=new cWHSSearch();
                $szUserDidNotGetAnyOnlineService = $kWHSSearch->getManageMentVariableByDescription('__USER_DID_NOT_GET_ANY_ONLINE_SERVICES__');
    
                $res_ary=array(); 
                $res_ary['szInternalComment'] = $szUserDidNotGetAnyOnlineService ;
                if(!empty($res_ary))
                {
                    $update_query = '';
                    foreach($res_ary as $key=>$value)
                    {
                        $update_query .= $key." = '".mysql_escape_custom(trim($value))."'," ;
                    }
                } 
                $update_query = rtrim($update_query,",");
                if($kBooking->updateDraftBooking($update_query,$idBooking))
                {

                }
                if($iPageVersion==3)
                {
                    echo __BOOKING_GET_QUOTE_PAGE_URL__."/".$szBookingRandomNum."/".__QUOTATION_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
                    die;
                } 
                else
                {
                    echo __BOOKING_GET_QUOTE_PAGE_URL__."/".$szBookingRandomNum."/".__QUOTATION_PAGE_URL_SUFFIX__."-".$szCountryStrUrl."/" ;
                    die;
                } 
            } 
        } 
        die; 
    }
    else if($kConfig->iCargoExceed==1)
    {
        $kBooking=new cBooking();
        $szBookingRandomNum = $kConfig->szBookingRandomNum;
        $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);

        if($idBooking>0)
        {
            $kBooking->save($kConfig,$idBooking);
        }
        else
        {
            $kBooking->add($kConfig,__BOOKING_STATUS_ZERO_LEVEL__);
            $idBooking = $kBooking->idBooking;
        }

        $postSearchAry = $kBooking->getBookingDetails($idBooking);
        $_SESSION['cargo_details_'.$idBooking] = $kConfig->tempCargoAry ;

        echo "CARGO_EXCEED_POPUP||||";
        echo display_cargo_exceed_popup($postSearchAry);
        die;
    }
    else if($kConfig->iDangerCargoFlag)
    {
            $kBooking=new cBooking();
            $szBookingRandomNum = $kConfig->szBookingRandomNum;
            $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);

            if($idBooking>0)
            {
                    $kBooking->save($kConfig,$idBooking);
            }
            else
            {
                    $kBooking->add($kConfig,__BOOKING_STATUS_ZERO_LEVEL__);
                    $idBooking = $kBooking->idBooking;
            }

            $postSearchAry = $kBooking->getBookingDetails($idBooking);
            $_SESSION['cargo_details_'.$idBooking] = $kConfig->tempCargoAry ; 

            echo "CARGO_EXCEED_POPUP||||";
            echo display_dangerous_cargo_popup($postSearchAry);
            die;
    }
    else if(!empty($kConfig->multiEmaptyPostcodeFromCityAry) || !empty($kConfig->multiEmaptyPostcodeToCityAry))
    {
            echo "MULTI_EMPTY_POSTCODE||||" ;

            $kBooking=new cBooking();
            $szBookingRandomNum = $kConfig->szBookingRandomNum;
            $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);

            if($idBooking>0)
            {
                    /*
                     * if($kConfig->szBookingMode==2) //repeat booking 
                    {
                            //repeat booking 
                            $kBooking->add($kConfig);
                            $idBooking = $kBooking->idBooking;
                    }
                    else
                    {
                            // update booking details
                            $kBooking->save($kConfig,$idBooking);
                    }	

                     * */

                    // update booking details
                    $kBooking->save($kConfig,$idBooking);
            }
            else
            {
                    $kBooking->add($kConfig);
            }
            $header_text = t($t_base.'fields/empty_postcode_origin');
            $from_postcode = false;
            if(!empty($kConfig->multiEmaptyPostcodeFromCityAry))
            {
                    $header_text .= " ".t($t_base.'fields/origin');
                    $from_postcode = true;
            }		

            if(!empty($kConfig->multiEmaptyPostcodeToCityAry))
            {
                    if($from_postcode)
                    {
                            $header_text .=" ".t($t_base.'fields/and')." ".t($t_base.'fields/destination');
                    }
                    else
                    {
                            $header_text .=" ".t($t_base.'fields/destination');
                    }
            }
            ?>
            <div id="popup-bg"></div>
            <form name="multi_empty_postcode_from_form" id="multi_empty_postcode_from_form" action="" method="post">
            <div id="popup-container">
            <div class="multi-postcode-parent-popup popup">		
            <H5><?=$header_text?></H5>
            <div id="popupError" class="errorBox" style="display:none;">
            </div>		
            <?php
            if(!empty($kConfig->multiEmaptyPostcodeFromCityAry))
            {	
                    $counterFrom = count($kConfig->multiEmaptyPostcodeFromCityAry);
                    if($counterFrom>5) // if there are more then five options available. 
                    {
                            //please put your styling in following variable 

                            $div_class = 'class="multi-postcode-popup" style="padding:10px;border:solid 1px #c4bda1;width:430px;"';
                    }
                    else
                    {
                            //$div_class = 'class="compare-popup popup"';
                    }
                    ?>
                    <p><?=t($t_base.'messages/which_part_of');?> <?=$kConfig->szOriginCity?> <?=t($t_base.'messages/need_transportation');?></p>
                    <br />
                    <div <?=$div_class?>>				
                            <?php
                                    foreach($kConfig->multiEmaptyPostcodeFromCityAry as $key=>$fromRegions)
                                    {
                            ?>
                                    <p><input type="radio" name="multiEmptyPostcodeCityAry[szRegionFrom]" value="<?=$key?>">&nbsp;<?=$fromRegions?></p>
                            <?
                                    }
                    ?>
                            <input type="hidden" name="multiEmptyPostcodeCityAry[iRegionFrom]" value="1">
                    </div>
                    <?php
                    $from_true = true;
            }	 
            if(!empty($kConfig->multiEmaptyPostcodeToCityAry))
            {		
                    $counterFrom = count($kConfig->multiEmaptyPostcodeToCityAry);
                    $div_class = '';
                    if($counterFrom>5) // if there are more then five options available. 
                    {
                            //please put your styling in following variable 
                            $div_class = 'class="multi-postcode-popup" style="padding:10px;border:solid 1px #c4bda1;width:430px;"';
                    }
                    else
                    {
                            //$div_class = 'class="compare-popup popup"';
                    }
                    if($from_true)
                    {
                            echo '<br />';
                    }
                    ?>	
                    <p><?=t($t_base.'messages/which_part_of');?> <?=$kConfig->szDestinationCity?> <?=t($t_base.'messages/need_transportation_to');?></p>
                    <br>		
                    <div <?=$div_class?>>				
                            <?php
                                    foreach($kConfig->multiEmaptyPostcodeToCityAry as $key=>$fromRegions)
                                    {
                            ?>
                                    <p><input type="radio" name="multiEmptyPostcodeCityAry[szRegionTo]" value="<?=$key?>">&nbsp;<?=$fromRegions?></p>
                            <?php
                                    }
                            ?>
                            <input type="hidden" name="multiEmptyPostcodeCityAry[iRegionTo]" value="1">
                            </div>
                            <?
              }
            ?>
            <br/>
            <p align="center">
                    <input type="hidden" name="multiEmptyPostcodeCityAry[szBookingRandomNum]" id="szBookingRandomNum" value="<?=$kConfig->szBookingRandomNum?>">
                            <a href="javascript:void(0);" onclick="updateEmptyPostcodeCity('multi_empty_postcode_from_form')" class="button1">
                            <span><?=t($t_base.'fields/select');?></span></a>
                            <a href="javascript:void(0);" onclick="showHide('Transportation_pop')" class="button2">
                            <span><?=t($t_base.'fields/cancel');?></span></a>
            </p>
            </div>
            </div>
	</form>	
            <?php
	}
	else if(!empty($kConfig->multiRegionToCityAry) || !empty($kConfig->multiRegionFromCityAry))
	{
		echo "MULTIREGIO||||";
		$kBooking=new cBooking();
		$szBookingRandomNum = $kConfig->szBookingRandomNum;
		$idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
		if($idBooking>0)
		{
                    // update booking details
                    $kBooking->save($kConfig,$idBooking);
		}
		else
		{
                    $kBooking->add($kConfig);
                    $idBooking = $kBooking->idBooking;
		}
		?>
		<div id="popup-bg"></div>
		<form name="multiregion_select_form" id="multiregion_select_form" action="" method="post">
		<div id="popup-container">
		<div class="compare-popup popup" style="margin-top:-30px;">
		<?php
			if(!empty($kConfig->multiRegionFromCityAry))
			{
                            ?>			
                            <H5><?=t($t_base.'fields/from_regions');?></H5>
                            <p><?=t($t_base.'messages/from_regions_popup_header');?></p>
                            <?php foreach($kConfig->multiRegionFromCityAry as $key=>$fromRegions) { ?>
                                <p> <input type="radio" checked name="multiregionCityAry[szRegionFrom]" value="<?=$key?>">&nbsp;<?=$fromRegions?></p>
                            <?php }
			}
		?>
		<?php
                    if(!empty($kConfig->multiRegionToCityAry))
                    {
                        ?>		
                        <br>	
                        <H5><?=t($t_base.'fields/to_regions');?></H5>
                        <p><?=t($t_base.'messages/to_regions_popup_header');?></p>
                        <?php
                                foreach($kConfig->multiRegionToCityAry as $key=>$fromRegions)
                                {
                        ?>
                                <p> <input type="radio" checked name="multiregionCityAry[szRegionTo]" value="<?=$key?>">&nbsp;<?=$fromRegions?></p>
                        <?php
                                }
                        ?>
                        <?php
                    }
		?>
		<br/>
		<p align="center">
                    <input type="hidden" name="multiregionCityAry[szBookingRandomNum]" value="<?=$kConfig->szBookingRandomNum?>">
                    <a href="javascript:void(0);" onclick="updatePostcodeRegion();" class="button1">
                    <span><?=t($t_base.'fields/continue');?></span></a>
                </p>
                </div>
            </div>
	</form>
        <?php
    }
    else
    {
        $kBooking=new cBooking();

        $szBookingRandomNum = $kConfig->szBookingRandomNum;
        $idBooking = $kBooking->getBookingIdByRandomNum($szBookingRandomNum);
        if($idBooking>0)
        {
            // update booking details
            $kBooking->save($kConfig,$idBooking);
        }
        else
        {
            $kBooking->add($kConfig);
            $idBooking = $kBooking->idBooking;
        }

        $kWhsSearch = new cWHSSearch();
        $postSearchAry = $kBooking->getBookingDetails($idBooking);
        $iAvailableServiceCount = $kWhsSearch->getAvailableService($postSearchAry['idOriginCountry'],$postSearchAry['idDestinationCountry'],true);

        if($iAvailableServiceCount==0)
        {
            $kBooking->addExpactedServiceData($idBooking);
            $landing_page_url = __LANDING_PAGE_URL__ ;
            $idExpactedService = $kBooking->idExpactedService ;
            $t_base = "LandingPage/";
            echo "NO_SERVICE_POPUP||||";
            echo display_no_service_available_popup($t_base,$landing_page_url,$idBooking,$idExpactedService,$kBooking,$postSearchAry);
        }
        else
        {
            echo "SUCCESS ||||";
        }
    }
}

?>