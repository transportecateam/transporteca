<?php
ob_start();
session_start();
if( !defined( "__APP_PATH__" ) )
define( "__APP_PATH__", realpath( dirname( __FILE__ ) . "/" ) );
require_once( __APP_PATH__ . "/inc/constants.php" );
require_once( __APP_PATH_LAYOUT__ . "/ajax_header.php" );

$t_base = "Users/AccountPage/";
$redirect_url = sanitize_all_html_input(trim($_REQUEST['redirect_url']));
$KwarehouseSearch=new cWHSSearch();

$iLanguage = getLanguageId();
//if($iLanguage==__LANGUAGE_ID_DANISH__)
//{	
//	$benefits=$KwarehouseSearch->getManageMentVariableByDescription(__BENEFITS_OF_SIGN_IN_DANISH__);
//}
//else
//{
	$benefits=$KwarehouseSearch->getManageMentVariableByDescription(__BENEFITS_OF_SIGN_IN__,$iLanguage);
//
$benefits = html_entity_decode($benefits);
$flag = sanitize_all_html_input(trim($_REQUEST['flag']));
$mode = sanitize_all_html_input(trim($_REQUEST['mode']));
if($flag==CREATE_ACCOUNT_WHY)
{
	$close_onclick = "showHide('ajaxLogin')";
	$class_name = 'class="popup forgot-password-popup signin-popup-verification"';
}
else
{
	if($mode=='CLICKED_ON_BOOK')
	{
		$close_onclick = "close_benefit_popup('".$redirect_url."','".$mode."')";		
		$class_name ='class="compare-popup popup"';
	}
	else 
	{
		$close_onclick = "close_benefit_popup('".$redirect_url."')";
		$class_name = 'class="popup forgot-password-popup signin-popup-verification"';
	}
} 
?>
<div id="popup-bg"></div>
<div id="popup-container">
<div <?=$class_name?>>
<p class="close-icon" align="right">
<a onclick="<?=$close_onclick?>" href="javascript:void(0);">
<img alt="close" src="<?=__BASE_STORE_IMAGE_URL__?>/close1.png">
</a>
</p>
			<h5><strong><?=t($t_base.'messages/benefit_of_signing');?></strong></h5>
			<? if($benefits){echo refineDescriptionData($benefits);}?>
			<br />
			<p align="center">
				<a href="javascript:void(0)" class="button1" onclick="<?=$close_onclick?>"><span><?=t($t_base.'fields/close');?></span></a>
			</p>
		</div>
</div>
